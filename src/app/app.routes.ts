//LIBRERIAS
import { RouterModule, Routes} from "@angular/router"


//COMPONENTES
import { NoencontradaComponent } from "./Menu/Noencontrada/noencontrada.component"
import { AutenticaGuard } from "./Infraestructura/Shell/Guardian/autentica.guard"
import { LoginComponent } from "./Infraestructura/Shell/Pagina/login/login.component";
import { PrincipalComponent } from "./Menu/Principal/principal/principal.component"

// import { AutenticaGuard } from "./Guardian/autentica.guard";

//RUTAS 
const incialRutas: Routes = [
    {path:'login', component: LoginComponent},
    {path:'principal', component: PrincipalComponent,canActivate: [AutenticaGuard]},
    {path:'', redirectTo: '/login', pathMatch: 'full' },
    {path:'**', component: NoencontradaComponent },
]

export const RUTA_INICIAL = RouterModule.forRoot(incialRutas, { useHash: true } );
