import { Component, OnInit, Input } from '@angular/core';
import { Compania, Departamento } from '../../../Infraestructura/Entidad/entidad.indice';
import { UbicacionService } from '../../Servicio/ubicacion.service';
import { AmbienteUbicacion } from '../../../Infraestructura/Shell/Modelo/ambiente-ubicacion';


@Component({
  selector: 'app-ubicacion',
  templateUrl: './ubicacion.component.html',
  styleUrls: ['./ubicacion.component.css']
})
export class UbicacionComponent implements OnInit {

 
  private _AmbienteUbicacion : AmbienteUbicacion;
  public get AmbienteUbicacion() : AmbienteUbicacion {
   return this._AmbienteUbicacion;
  }
  public set AmbienteUbicacion(ambiente : AmbienteUbicacion) {
    this._AmbienteUbicacion = ambiente;
  }

  private _IdAmbiente : string;
  public get IdAmbiente() : string {
    return this._IdAmbiente;
  }
  public set IdAmbiente(Id : string) {
    this._IdAmbiente = Id;
    this._AmbienteUbicacion = this.ubicacionService.obtiene(this._IdAmbiente);
  }

  public ListaCompanias: Array<Compania>=[];
  public ListaDepartamentos: Array<Departamento>=[];


  @Input() Id: string = '';
  @Input() Titulo: string = '';

  constructor(public ubicacionService: UbicacionService) {

  }

  ngOnInit() {
  }

  ngOnChanges() {

    this.IdAmbiente = this.Id; 

    //RECUPERA AMBIENTE-LOCAL
    if (!this.AmbienteUbicacion)
       this.AmbienteUbicacion = new AmbienteUbicacion();


    //RECUPERA LISTAS-BDD
    this.ListaCompanias = this.ubicacionService.getListaCompanias();
    this.ListaDepartamentos = this.ubicacionService.getListaDepartamentos();


  }

  onCambioCompania(compania:string) {

    
    //Recupera ID
    this.AmbienteUbicacion.Compania=compania;
    this.AmbienteUbicacion.IdCompania = this.ListaCompanias.find(x => x.NombreCorto==compania).IdCompania;
    
    //Guardad Preferencia
    this.ubicacionService.guarda(this.IdAmbiente,this.AmbienteUbicacion);
 
  }

  onCambioDepartamento(departamento:string) {

    //Guardad Preferencia
    this.AmbienteUbicacion.Departamento=departamento;
    this.AmbienteUbicacion.IdDepartamento=this.ListaDepartamentos.find(x=>x.NombreDepartamento==departamento).IdDepartamento
    
    //Guardad Preferencia
    this.ubicacionService.guarda(this.IdAmbiente,this.AmbienteUbicacion);
 
  }
  




}
