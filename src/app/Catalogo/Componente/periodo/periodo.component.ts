import { Component, OnInit, AfterViewInit, Input, OnChanges, OnDestroy } from '@angular/core';

import { PeriodoService } from '../../Servicio/periodo.service';

//ENTIDAD - MODELO
import { Periodo } from '../../../Infraestructura/Entidad/entidad.indice';
import { AmbientePeriodo } from '../../../Infraestructura/Shell/Modelo/ambiente-periodo';
import { PreferenciaService } from '../../../Infraestructura/Shell/Servicio/servicio.indice';

//SERVICIO-SHELL
@Component({
  selector: 'app-periodo',
  templateUrl: './periodo.component.html',
  styleUrls: ['./periodo.component.css']
})
export class PeriodoComponent implements OnInit, OnChanges, OnDestroy {
 
  private _AmbientePeriodo : AmbientePeriodo;
  public get AmbientePeriodo() : AmbientePeriodo {
   return this._AmbientePeriodo;
  }
  public set AmbientePeriodo(ambiente : AmbientePeriodo) {
    this._AmbientePeriodo = ambiente;
  }


  private _IdAmbiente : string;
  public get IdAmbiente() : string {
    return this._IdAmbiente;
  }
  public set IdAmbiente(Id : string) {
    this._IdAmbiente = Id;
    this._AmbientePeriodo = this.periodoService.obtiene(this._IdAmbiente);
  }



  public ListaPeriodos: Array<Periodo>=[];
  public ListaAnio: Array<Periodo>=[];

  @Input() Id: string = '';
  @Input() Titulo: string = '';

  constructor(public periodoService: PeriodoService) {
    
  }

  ngOnInit() {
  
  }

  ngOnChanges() {

      //RECUPERA AMBIENTE-LOCAL
      this.IdAmbiente = this.Id; 
      if (!this.AmbientePeriodo) {
        this.AmbientePeriodo= new AmbientePeriodo();
      }
      else { //FILTRADO CON EL ANO RECUPERADO
        this.ListaPeriodos= this.periodoService.getListaPeriodoPorAnio(this.AmbientePeriodo.Anio);
      }

      //RECUPERA LISTAS-BDD
      this.ListaAnio = this.periodoService.getListaAnio();

  }

  ngOnDestroy() {

    this.periodoService.guarda(this.IdAmbiente,this.AmbientePeriodo);
    console.log("Guardando => ", this.AmbientePeriodo);

  }

  onCambioAnio(anio:number) {

    //Filtra
    this.AmbientePeriodo.Anio=Number(anio);
    this.ListaPeriodos = this.periodoService.getListaPeriodoPorAnio(anio);
   
    //Guarda preferencia
    this.periodoService.guarda(this.IdAmbiente,this.AmbientePeriodo);
 
  }

  onCambioMes(mes:string) {
   
    //Obtiene el IdPeriodo
    this.AmbientePeriodo.Mes=mes;
    this.AmbientePeriodo.IdPeriodo = this.ListaPeriodos.find(x => x.Descripcion== mes && x.Anio==this.AmbientePeriodo.Anio).IdPeriodo;

    //Guarda Preferencia
    this.periodoService.guarda(this.IdAmbiente,this.AmbientePeriodo);
    // console.log("Preferencia ", this.AmbientePeriodo);

  }

  mostrar() {
    var ambiente: AmbientePeriodo = this.AmbientePeriodo;
    alert('Mes => ' + ambiente.Mes + " Anio => " + ambiente.Anio + " IdPeriodo => " + ambiente.IdPeriodo);

  }


}
