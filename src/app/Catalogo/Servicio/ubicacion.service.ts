
//LIBRERIAS ANGULAR
import { Injectable } from '@angular/core';
import { asEnumerable } from 'linq-es5';

//ENTIDADES
import { Compania, Departamento } from '../../Infraestructura/Entidad/entidad.indice';

//SERVICIOS
import { GestorCacheService } from '../../Compartido/GestorCache/gestor-cache.service';
import { PreferenciaService } from '../../Infraestructura/Shell/Servicio/servicio.indice';

//CONSTANTES
import { CONSTANTE } from '../../Infraestructura/Constante/constante';


@Injectable({
  providedIn: 'root'
})
export class UbicacionService {

  constructor(public gestorCache: GestorCacheService, private preferenciaService: PreferenciaService) { }

   
  getIdAmbiente(Id:string):string {
    Id = this.preferenciaService.IdAmbiente(Id,"Grupo");
    return Id;
    
  }


 //COMPANIAS
 getListaCompanias(): Array<Compania> {
  return this.gestorCache.DatosBarraCia.ListaCompanias;
}

getListaCompania(): Array<Compania> {

  var ListaCompanias: Array<Compania>;
  
  //Recupera Companias
  ListaCompanias = this.gestorCache.DatosBarraCia.ListaCompanias;

  //Recupera-Departamento-Ordenado
  return ListaCompanias;
}

getListaDepartamentos(): Array<Departamento> {
  var ListaDepartamentos: Array<Departamento>;
  
  //Obtiene Departamentos
  ListaDepartamentos = this.gestorCache.DatosBarraCia.ListaDepartamentos;
  
  //Ordena 
  return asEnumerable(ListaDepartamentos).OrderBy(p => p.NombreDepartamento).ToArray();
}


getIdGrupoDeCompania():string {
  return this.gestorCache.DatosBarraCia.ListaCompanias[CONSTANTE.INDICE_PRIMERA_COMPANIA].IdGrupo;
  
}


guarda(clave:string, objeto: any):void {
  this.preferenciaService.guarda('Grupo',clave,objeto);

 }

 obtiene(clave:string): any {
  let objeto = this.preferenciaService.obtiene('Grupo',clave);
  return objeto;
 }



}
