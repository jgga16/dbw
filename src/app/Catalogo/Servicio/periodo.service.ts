import { Injectable } from '@angular/core';

import { asEnumerable } from 'linq-es5';

import { Periodo } from '../../Infraestructura/Entidad/periodo';
import { GestorCacheService } from '../../Compartido/GestorCache/gestor-cache.service';
import { PreferenciaService } from '../../Infraestructura/Shell/Servicio/servicio.indice';


@Injectable({
  providedIn: 'root'
})
export class PeriodoService {
 
 
  
  constructor(public gestorCache: GestorCacheService, private preferenciaService: PreferenciaService) { }

  getIdAmbiente(Id:string):string {
    Id = this.preferenciaService.IdAmbiente(Id,"Compania");
    return Id;
    
  }
  
  //PERIODOS
  getListaPeriodos(): Array<Periodo> {
    return this.gestorCache.DatosBarraCia.ListaPeriodos;
  }

  getListaAnio(): Array<Periodo> {

    var ListaPeriodos: Array<Periodo>;
    
    //Recupera Periodo del cache
    ListaPeriodos = this.gestorCache.DatosBarraCia.ListaPeriodos;

    //Recupera-Anio
    return asEnumerable(ListaPeriodos).Distinct(x => x.Anio).ToArray();;
  }


  getListaPeriodoPorAnio(anio:number): Array<Periodo> {
    var ListaPeriodos: Array<Periodo>;
    
    //Filtra mes por anio seleccionado
    ListaPeriodos = this.gestorCache.DatosBarraCia.ListaPeriodos.filter(p => p.Anio == anio);
    //Ordena 
    return asEnumerable(ListaPeriodos).OrderBy(p => p.NumeroPeriodo).ToArray();
  }
  


  guarda(clave:string, objeto: any):void {
    this.preferenciaService.guarda('Compania',clave,objeto);

   }
  
   obtiene(clave:string): any {
    let objeto = this.preferenciaService.obtiene('Compania',clave);
    return objeto;
   }




}



