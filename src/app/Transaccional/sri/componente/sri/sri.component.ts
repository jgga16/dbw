import { Component, OnInit, Input, AfterContentInit, OnDestroy } from '@angular/core';
import { TabService } from '../../../../Menu/tab/tab.service';
import { AccionMensaje, CoordinacionMensaje, ConfiguracionComponente } from '../../../../Infraestructura/Shell/Modelo/shell-modelo.indice';
import { CoordinacionService } from '../../../../Infraestructura/Shell/Servicio/coordinacion.service';
import { RespuestaGrabarTransaccion, FacturacionCabecera, BaseComponente, CommonErrorMessage } from '../../../../Infraestructura/Entidad/entidad.indice';
import { Subscription, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { EstadoComponente } from '../../../../Infraestructura/Entidad/estadocomponente';



@Component({
  selector: 'app-sri',
  templateUrl: './sri.component.html',
  styleUrls: ['./sri.component.css']
})
export class SriComponent extends BaseComponente implements OnInit, OnDestroy {
 
  //DATOS COMPONENTE
  public sriCabecera: FacturacionCabecera = new FacturacionCabecera();

  constructor(public tabService: TabService, public coodinacionService: CoordinacionService) {
    super();
   }

  ngOnInit() {

    //Inicializa datos de sri
    this.sriCabecera.IdCompania = '01';
    this.sriCabecera.Numero =22;
    this.sriCabecera.Siglas ='WILITO';

    //subscribirse Tab y Coordinador
    this.suscribirseTab();
    this.suscribirseRespuesta();
  }

  ngOnDestroy(){
    super.ngOnDestroy(); //Destruir referencias
  }

  //SUBSCRIPCION TAB
  suscribirseTab():void{
    this.refTab = this.tabService.comando$.subscribe(
      resp => this.respTab(resp),
      err => this.errTab(err),
      () => this.compTab()
    );
  } //fin suscribirseTab

  respTab(resp) {
    let accion: AccionMensaje = resp as AccionMensaje;
     
    if (this.idTab != accion.idTab) return;
    console.log("      *** CMP-SRI idTab-idComponente-Orden-Estado => " + this.idTab + "-" + this.estadoComponente.idComponente + "-" + this.estadoComponente.orden + "-" + this.estadoComponente.estado);
  
    //  debugger;
    //EJECUTAR COMANDO y CAMBIA ESTADO
    if (this.ejecutaComando(accion)) {
    }
  }

  errTab(err){
     alert('sri => error ' + err);
  }

  compTab(){
   
  }
  
  //Envia mensaje al coordinador
  enviaCoordinador(coordinacion: CoordinacionMensaje):boolean {
    this.coodinacionService.captarComponente(coordinacion);
    return true;
  }


  //SUBSCRIPCION COORDINACION

  suscribirseRespuesta():void{
   
    //var Respuesta$  = this.coodinacionService._respuestaSubject.pipe(filter( (res) => res.IdTab== this.idTab ));
    this.Respuesta$ = this.coodinacionService.Respuesta$; //.pipe(filter( (res) => res.IdTab== this.idTab ));      /// filter(res => res.endsWith(idTab.toString());

    this.refRespuesta = this.Respuesta$.subscribe(
      resp => this.respCoordinador(resp),
      err => this.errCoordinador(err),
      () => this.compCoordinador()
    );

  } //fin suscribirseRespuesta


  respCoordinador(resp) {
    let respuesta: RespuestaGrabarTransaccion = resp as RespuestaGrabarTransaccion;
    // debugger; 
    if (this.idTab!=respuesta.IdTab) return 
    
    if (this.procesaRespuesta(respuesta)) {

    }

  }

  errCoordinador(err){
    alert('CMP - sri => error ' + err);
  }

  compCoordinador(){
  }



  //Crear el mensaje de coordinacion con el servicion de coordinacion
  crearMensajeCoordinacion(accion: AccionMensaje):CoordinacionMensaje {
    let coodinacionMsj: CoordinacionMensaje = new CoordinacionMensaje();
    
    this.estadoComponente.idTab = this.idTab;

    coodinacionMsj.Accion= accion;
    coodinacionMsj.Dato = this.sriCabecera;
    coodinacionMsj.EstadoComponente = this.estadoComponente;
    return coodinacionMsj;
  }
 

  procesaRespuesta(respuesta: RespuestaGrabarTransaccion):boolean {
    console.log('       <- sri-Cmp => respuesta ' , respuesta);
    //CAMBIAR ESTADO
    if (respuesta.Errores)
      this.cambiaEstado('grabar')
    else
      this.cambiaEstado('nuevo')  

    return true;

  }

  ejecutaComando(accion: AccionMensaje):boolean {
    //validar datos
    //mostrar errores
    //ejecutar
    console.log('       ** sri-Cmp => procesando ' , accion.accion);   
    if (accion.accion=='grabar') {

      this.coordinacionMensaje = this.crearMensajeCoordinacion(accion);
      if (this.enviaCoordinador(this.coordinacionMensaje)) {
        if (this.cambiaEstado(accion.accion)) {
          return true;
        }
      }
    } //grabar
      

    if (accion.accion=='cancelar') {

      if (this.cambiaEstado(accion.accion)) {
        return true;
      }
    } //cancelar

    if (accion.accion=='editar') {

      if (this.cambiaEstado(accion.accion)) {
        return true;
      }
    } //editar

    if (accion.accion=='salir') {

      if (this.cambiaEstado(accion.accion)) {
        return true;
      }
    } //salir

    if (accion.accion=='nuevo') {

      if (this.cambiaEstado(accion.accion)) {
        return true;
      }
    } //nuevo

    console.log('       ** sri-Cmp => ACCION DESCONOCIDA ' , accion.accion);
    return false;

  }
  almacenaError(tipo:CommonErrorType, texto:string, property:string ):void {
    let errorMensaje: CommonErrorMessage = new CommonErrorMessage();
    
    errorMensaje.ErrorType= CommonErrorType.InvalidFormat,
    errorMensaje.ErrorText= 'Error: componete SRI',
    errorMensaje.ErrorProperty= 'grabar'
    this.Errores.push(errorMensaje);

  }
  //GENERAR ESTADO DE PRUEBAS
  onClick(accion) {

    if (accion=='error') {
      this.cambiaEstado(EstadoComponenteTipo.error);
      this.almacenaError(CommonErrorType.InvalidFormat,'Error: componete SRI','presiono simulacion E');
    }

    if (accion=='valido') {
      this.cambiaEstado(EstadoComponenteTipo.editar);
      this.Errores = [];
    }
  }



}
