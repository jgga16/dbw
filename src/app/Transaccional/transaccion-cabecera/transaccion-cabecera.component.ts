import { Component, OnInit, OnDestroy } from '@angular/core';
import { BaseComponente } from '../../Infraestructura/Entidad/base-componente';
import { TabService } from '../../Menu/tab/tab.service';
import { CoordinacionService } from '../../Infraestructura/Shell/Servicio/servicio.indice';
import { TransaccionCabecera, RespuestaGrabarTransaccion, CommonErrorMessage } from '../../Infraestructura/Entidad/entidad.indice';
import { AccionMensaje } from '../../Infraestructura/Shell/Modelo/accion-mensaje';
import { CoordinacionMensaje } from '../../Infraestructura/Shell/Modelo/shell-modelo.indice';

@Component({
  selector: 'app-transaccion-cabecera',
  templateUrl: './transaccion-cabecera.component.html',
  styleUrls: ['./transaccion-cabecera.component.css']
})
export class TransaccionCabeceraComponent extends BaseComponente implements OnInit, OnDestroy {
  public transaccionCabecera: TransaccionCabecera;
  constructor(public tabService: TabService, public coordinacionSrv: CoordinacionService) {
    super();
   }

  ngOnDestroy(){
    super.ngOnDestroy(); //Destruir referencias
  }

  InicializaTransaccion():boolean {
    
    this.transaccionCabecera = new TransaccionCabecera();
    this.transaccionCabecera.IdCompania='C37E7C16-52DA-4E58-A80F-3842E880394A';
    this.transaccionCabecera.IdUsuario= '209831CF-1E06-44C1-8E4D-00ADC1F70005';

    this.transaccionCabecera.Anio= 2018;
    this.transaccionCabecera.Periodo= 3;
    this.transaccionCabecera.Dia= 23;


      this.transaccionCabecera.IdTransaccionCabecera="";
      this.transaccionCabecera.IsPendienteFe= false;
      this.transaccionCabecera.Siglas= 'FC';
      this.transaccionCabecera.Numero= 9899;
      this.transaccionCabecera.IdProceso= '41F6D1E2-0D66-404F-9CD9-EA8E8B6BE625';
      this.transaccionCabecera.IdProcesoHistoria= '41F6D1E2-0D66-404F-9CD9-EA8E8B6BE625';
      this.transaccionCabecera.Fecha= '20180323'; //DateTimeOffset;
      this.transaccionCabecera.FechaValor= '20180323'; //dateTime;
      this.transaccionCabecera.ContadorImpresion= 0;
      

      this.transaccionCabecera.IdUsuarioHistoria= 'F085C7C5-6930-4AF7-A6DB-9EEA2F20B4AA';
      this.transaccionCabecera.IdSriAutorizacionCabecera= null;
      this.transaccionCabecera.IdSriPuntoEmision= null;
      this.transaccionCabecera.IdSriPuntoEmisionHistoria= null;
      this.transaccionCabecera.IsTrsReversa= false;
      this.transaccionCabecera.IsAnulado= false;
      this.transaccionCabecera.IsCopiaAutomatica= false;
      this.transaccionCabecera.IsReversado= false;
      this.transaccionCabecera.AnuladoInterno= false;
      return true;
  }
  ngOnInit() {

    if (this.InicializaTransaccion()) {

    }

    //subscribirse Tab y Coordinador
    this.suscribirseTab();
    this.suscribirseRespuesta();
  }

 

//#SUBSCRIPCION TAB
suscribirseTab():void{
  this.refTab = this.tabService.comando$.subscribe(
    resp => this.respTab(resp),
    err => this.errTab(err),
    () => this.compTab()
  );
} //fin suscribirseTab

//#Manejador de la respuesta del Tab
respTab(resp) {
  let accion: AccionMensaje = resp as AccionMensaje;
   
  if (this.idTab != accion.idTab) return;
  console.log("      *** CMP-TRS idTab-idComponente-Orden-Estado => " + this.idTab + "-" + this.estadoComponente.idComponente + "-" + this.estadoComponente.orden + "-" + this.estadoComponente.estado);

  //EJECUTAR COMANDO y CAMBIA ESTADO
  if (this.ejecutaComando(accion)) {
  }
}

//#Manejador del error en respuesta del Tab
errTab(err){
   alert('TRS => error ' + err);
}

//#Manejador del complete en respuesta del Tab
compTab(){
 
}
//# FIN SUBSCRIPCION TAB

//Envia mensaje al coordinador
enviaCoordinador(coordinacion: CoordinacionMensaje):boolean {
  this.coordinacionSrv.captarComponente(coordinacion);
  return true;
}



//#SUBSCRIPCION COORDINACION

suscribirseRespuesta():void{
 
  //var Respuesta$  = this.coordinacionSrv._respuestaSubject.pipe(filter( (res) => res.IdTab== this.idTab ));
  this.Respuesta$ = this.coordinacionSrv.Respuesta$; //.pipe(filter( (res) => res.IdTab== this.idTab ));      /// filter(res => res.endsWith(idTab.toString());

  this.refRespuesta = this.Respuesta$.subscribe(
    resp => this.respCoordinador(resp),
    err => this.errCoordinador(err),
    () => this.compCoordinador()
  );

} //fin suscribirseRespuesta


//#Manejador de RESPUESTA del Coordinador

respCoordinador(resp) {
  let respuesta: RespuestaGrabarTransaccion = resp as RespuestaGrabarTransaccion;
  // debugger; 
  if (this.idTab!=respuesta.IdTab) return 
  
  if (this.procesaRespuesta(respuesta)) {

  }

}

//#Manejador de ERROR del Coordinador
errCoordinador(err){
  alert('CMP - TRS => error ' + err);
}

//#Manejador de COMPLETE del Coordinador
compCoordinador(){
}



//#Crear mensaje de coordinacion
crearMensajeCoordinacion(accion: AccionMensaje):CoordinacionMensaje {
  let coordinacionMsj: CoordinacionMensaje = new CoordinacionMensaje();
  
  this.estadoComponente.idTab = this.idTab;

  coordinacionMsj.Accion= accion;
  coordinacionMsj.Dato = this.transaccionCabecera;
  coordinacionMsj.EstadoComponente = this.estadoComponente;
  coordinacionMsj.Errores = this.Errores;
  return coordinacionMsj;
}

//#Crear mensaje de coordinacion


almacenaError(tipo:CommonErrorType, texto:string, property:string ):void {
  let errorMensaje: CommonErrorMessage = new CommonErrorMessage();
  
  errorMensaje.ErrorType= CommonErrorType.InvalidFormat,
  errorMensaje.ErrorText= 'Error: componete TRS',
  errorMensaje.ErrorProperty= 'grabar'
  this.Errores.push(errorMensaje);

}

 //#Procesar respuesta de la transaccion
 procesaRespuesta(respuesta: RespuestaGrabarTransaccion):boolean {
  console.log('       <- TRS-Cmp => respuesta ' , respuesta);
  //CAMBIAR ESTADO
  if (respuesta.Errores) {
    this.cambiaEstado('error')
    this.Errores = respuesta.Errores; //cambiar esto cuado este la WebAPI
    this.almacenaError(CommonErrorType.InvalidFormat,'Error: componete TRS -servidor retorno error ','servidor retorno error');
  }  
  else {
    this.cambiaEstado('readonly')  

  }
  return true;
}

//#Ejecutar accion solicitada por la botonera principal
ejecutaComando(accion: AccionMensaje):boolean {
  //validar datos
  //mostrar errores
  //ejecutar
  console.log('       ** TRS-Cmp => procesando ' , accion.accion);   
  if (accion.accion=='grabar') {
    this.coordinacionMensaje = this.crearMensajeCoordinacion(accion);
    if (this.enviaCoordinador(this.coordinacionMensaje)) {
      if (this.cambiaEstado(EstadoComponenteTipo.editar)) {
        return true;
      }
    }
  } //grabar
    

  if (accion.accion=='cancelar') {

    if (this.cambiaEstado(EstadoComponenteTipo.readonly)) {
      return true;
    }
  } //cancelar

  if (accion.accion=='editar') {

    if (this.cambiaEstado(EstadoComponenteTipo.editar)) {
      return true;
    }
  } //editar

  if (accion.accion=='salir') {

    if (this.cambiaEstado(EstadoComponenteTipo.readonly)) {
      return true;
    }
  } //salir

  if (accion.accion=='nuevo') {

    if (this.cambiaEstado(EstadoComponenteTipo.editar)) {
      return true;
    }
  } //nuevo

  console.log('       ** TRS-Cmp => ACCION DESCONOCIDA ' , accion.accion);
  return false;

}




}
