import { Component, OnInit, Input, AfterContentInit, OnDestroy } from '@angular/core';
import { TabService } from '../../../../Menu/tab/tab.service';
import { AccionMensaje, CoordinacionMensaje, ConfiguracionComponente } from '../../../../Infraestructura/Shell/Modelo/shell-modelo.indice';
import { CoordinacionService } from '../../../../Infraestructura/Shell/Servicio/coordinacion.service';
import { RespuestaGrabarTransaccion, FacturacionCabecera, BaseComponente, CommonErrorMessage } from '../../../../Infraestructura/Entidad/entidad.indice';
import { Subscription, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { EstadoComponente } from '../../../../Infraestructura/Entidad/estadocomponente';


@Component({
  selector: 'app-factura',
  templateUrl: './factura.component.html',
  styleUrls: ['./factura.component.css']
})
export class FacturaComponent extends BaseComponente implements OnInit, OnDestroy {
 
  //DATOS COMPONENTE
  public facturaCabecera: FacturacionCabecera = new FacturacionCabecera();

  constructor(public tabService: TabService, public coordinacionSrv: CoordinacionService) {
    super();
   }


  ngOnInit() {

    //Inicializa datos de factura
    this.facturaCabecera.IdCompania = '01';
    this.facturaCabecera.Numero =22;
    this.facturaCabecera.Siglas ='WILITO';

    //subscribirse Tab y Coordinador
    this.suscribirseTab();
    this.suscribirseRespuesta();
  }

  ngOnDestroy(){
    super.ngOnDestroy(); //Destruir referencias
  }

  //#SUBSCRIPCION TAB
  suscribirseTab():void{
    this.refTab = this.tabService.comando$.subscribe(
      resp => this.respTab(resp),
      err => this.errTab(err),
      () => this.compTab()
    );
  } //fin suscribirseTab

  //#Manejador de la respuesta del Tab
  respTab(resp) {
    let accion: AccionMensaje = resp as AccionMensaje;
     
    if (this.idTab != accion.idTab) return;
    console.log("      *** CMP-FACT idTab-idComponente-Orden-Estado => " + this.idTab + "-" + this.estadoComponente.idComponente + "-" + this.estadoComponente.orden + "-" + this.estadoComponente.estado);

    //EJECUTAR COMANDO y CAMBIA ESTADO
    if (this.ejecutaComando(accion)) {
    }
  }

  //#Manejador del error en respuesta del Tab
  errTab(err){
     alert('Factura => error ' + err);
  }

  //#Manejador del complete en respuesta del Tab
  compTab(){
   
  }
  //# FIN SUBSCRIPCION TAB

  //Envia mensaje al coordinador
  enviaCoordinador(coordinacion: CoordinacionMensaje):boolean {
    this.coordinacionSrv.captarComponente(coordinacion);
    return true;
  }


  
  //#SUBSCRIPCION COORDINACION

  suscribirseRespuesta():void{
   
    //var Respuesta$  = this.coordinacionSrv._respuestaSubject.pipe(filter( (res) => res.IdTab== this.idTab ));
    this.Respuesta$ = this.coordinacionSrv.Respuesta$; //.pipe(filter( (res) => res.IdTab== this.idTab ));      /// filter(res => res.endsWith(idTab.toString());

    this.refRespuesta = this.Respuesta$.subscribe(
      resp => this.respCoordinador(resp),
      err => this.errCoordinador(err),
      () => this.compCoordinador()
    );

  } //fin suscribirseRespuesta


  //#Manejador de RESPUESTA del Coordinador

  respCoordinador(resp) {
    let respuesta: RespuestaGrabarTransaccion = resp as RespuestaGrabarTransaccion;
    // debugger; 
    if (this.idTab!=respuesta.IdTab) return 
    
    if (this.procesaRespuesta(respuesta)) {

    }

  }

  //#Manejador de ERROR del Coordinador
  errCoordinador(err){
    alert('CMP - Factura => error ' + err);
  }

  //#Manejador de COMPLETE del Coordinador
  compCoordinador(){
  }



  //#Crear mensaje de coordinacion
  crearMensajeCoordinacion(accion: AccionMensaje):CoordinacionMensaje {
    let coordinacionMsj: CoordinacionMensaje = new CoordinacionMensaje();
    
    this.estadoComponente.idTab = this.idTab;

    coordinacionMsj.Accion= accion;
    coordinacionMsj.Dato = this.facturaCabecera;
    coordinacionMsj.EstadoComponente = this.estadoComponente;
    coordinacionMsj.Errores = this.Errores;
    return coordinacionMsj;
  }
 
  //#Crear mensaje de coordinacion


  almacenaError(tipo:CommonErrorType, texto:string, property:string ):void {
    let errorMensaje: CommonErrorMessage = new CommonErrorMessage();
    
    errorMensaje.ErrorType= CommonErrorType.InvalidFormat,
    errorMensaje.ErrorText= 'Error: componete FACTURA',
    errorMensaje.ErrorProperty= 'grabar'
    this.Errores.push(errorMensaje);

  }

   //#Procesar respuesta de la transaccion
   procesaRespuesta(respuesta: RespuestaGrabarTransaccion):boolean {
    console.log('       <- Factura-Cmp => respuesta ' , respuesta);
    //CAMBIAR ESTADO
    if (respuesta.Errores) {
      this.cambiaEstado('error')
      this.Errores = respuesta.Errores; //cambiar esto cuado este la WebAPI
      this.almacenaError(CommonErrorType.InvalidFormat,'Error: componete FACTURA -servidor retorno error ','servidor retorno error');
    }  
    else {
      this.cambiaEstado('readonly')  

    }
    return true;
  }

  //#Ejecutar accion solicitada por la botonera principal
  ejecutaComando(accion: AccionMensaje):boolean {
    //validar datos
    //mostrar errores
    //ejecutar
    console.log('       ** Factura-Cmp => procesando ' , accion.accion);   
    if (accion.accion=='grabar') {
      this.coordinacionMensaje = this.crearMensajeCoordinacion(accion);
      if (this.enviaCoordinador(this.coordinacionMensaje)) {
        if (this.cambiaEstado(EstadoComponenteTipo.editar)) {
          return true;
        }
      }
    } //grabar
      

    if (accion.accion=='cancelar') {

      if (this.cambiaEstado(EstadoComponenteTipo.readonly)) {
        return true;
      }
    } //cancelar

    if (accion.accion=='editar') {

      if (this.cambiaEstado(EstadoComponenteTipo.editar)) {
        return true;
      }
    } //editar

    if (accion.accion=='salir') {

      if (this.cambiaEstado(EstadoComponenteTipo.readonly)) {
        return true;
      }
    } //salir

    if (accion.accion=='nuevo') {

      if (this.cambiaEstado(EstadoComponenteTipo.editar)) {
        return true;
      }
    } //nuevo

    console.log('       ** Factura-Cmp => ACCION DESCONOCIDA ' , accion.accion);
    return false;

  }

  //# Fin Ejecutar accion solicitada por la botonera principal



  //# Botones de prueba para cambiar estados
  onClick(accion) {

    if (accion=='error') {
      this.cambiaEstado(EstadoComponenteTipo.error);
      this.almacenaError(CommonErrorType.InvalidFormat,'Error: componete FACTURA','presiono simulacion E');
    }

    if (accion=='valido') {
      this.cambiaEstado(EstadoComponenteTipo.editar);
      this.Errores = [];
    }
  }
  //# Botones de prueba para cambiar estados


} //Fin Clase
