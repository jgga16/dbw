import { Injectable } from '@angular/core';

//ENTIDADES
import {
  MensajeBarraCia, DbWareMenuApi, Catalogos, Perfil, Usuario
} from '../../Infraestructura/Entidad/entidad.indice';

//ALMACENAMIENTO-LOCLA
import { LocalStorageService } from '../LocalStorage/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class GestorCacheService {


  //Mensaje-Login del servicio de autenticacion del servidor
  private _FechaProcesos: string;
  set FechaProcesos(FechaProcesos: string) {
    this._FechaProcesos = FechaProcesos;
    this.localStorage.guarda('FechaProcesos', FechaProcesos);

  }
  get FechaProcesos(): string {
    return this._FechaProcesos;
  }

  private _MensajeError: string;
  set MensajeError(MensajeError: string) {
    this._MensajeError = MensajeError;
  }
  get MensajeError(): string {
    return this._MensajeError;
  }

  private _SesionHash: string;
  set SesionHash(SesionHash: string) {
    this._SesionHash = SesionHash;
    this.localStorage.guarda('SesionHash', SesionHash);
  }
  get SesionHash(): string {

    return this._SesionHash;
  }

  private _MenuApi: Array<DbWareMenuApi>;
  set MenuApi(MenuApi: Array<DbWareMenuApi>) {
    this.localStorage.guarda('MenuApi', MenuApi);
    this._MenuApi = MenuApi;
  }
  get MenuApi(): Array<DbWareMenuApi> {
    return this._MenuApi;
  }


  private _Usuario: Usuario;
  set Usuario(Usuario: Usuario) {
    this._Usuario = Usuario;
    this.localStorage.guarda('Usuario', Usuario);

  }
  get Usuario(): Usuario {
    return this._Usuario;
  }

  private _Perfil: Perfil;
  set Perfil(Perfil: Perfil) {
    this._Perfil = Perfil;
    this.localStorage.guarda('Perfil', Perfil);
  }
  get Perfil(): Perfil {
    return this._Perfil;
  }


  private _Catalogos: Catalogos;
  set Catalogos(Catalogos: Catalogos) {
    this._Catalogos = Catalogos;
    this.localStorage.guarda('Catalogos', Catalogos);
  }
  get Catalogos(): Catalogos {
    return this._Catalogos;
  }

  private _DatosBarraCia: MensajeBarraCia;
  set DatosBarraCia(DatosBarraCia: MensajeBarraCia) {
    this._DatosBarraCia = DatosBarraCia;
    this.localStorage.guarda('DatosBarraCia', DatosBarraCia);
  }
  get DatosBarraCia(): MensajeBarraCia {
    return this._DatosBarraCia;
  }

  //Constructor
  constructor(public localStorage: LocalStorageService) { }

  //Refresca Cache
  refrescaCache() {
    return true;
  }

}
