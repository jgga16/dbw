import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  obtiene(clave: string, objetoDefecto: any) {
    let valor = localStorage.getItem(clave); 
    return (valor) ? JSON.parse(valor) : objetoDefecto;
  }
  
  guarda(clave: string, valor: any) {
    localStorage.setItem(clave, JSON.stringify(valor));
  }

}
