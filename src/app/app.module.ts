//Libreria
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { RouterModule, Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//Modelo
import { LoginComponent } from './Infraestructura/Shell/Pagina/login/login.component';
import { PrincipalComponent } from './Menu/Principal/principal/principal.component'
import { ErrorComponent } from './Menu/Error/error.component'
import { NoencontradaComponent } from './Menu/Noencontrada/noencontrada.component'


//RUTAS
import { InterceptorService } from './Infraestructura/Shell/Interceptor/interceptor.service';
import { AutenticacionService } from './Infraestructura/Shell/Servicio/autenticacion.service';
import { LateralComponent } from './Menu/lateral/lateral.component';
import { CabeceraComponent } from './Menu/cabecera/cabecera.component';

//MODULO
import { PrincipalModule } from './Menu/Principal/principal.module';
import { RUTA_INICIAL } from './app.routes';
import { PeriodoComponent } from './Catalogo/Componente/periodo/periodo.component';
import { UbicacionComponent } from './Catalogo/Componente/ubicacion/ubicacion.component';
import { TabComponent } from './Menu/tab/tab.component-new';
import { TabsComponent } from './Menu/tabs/tabs.component';
import { FacturaComponent } from './Transaccional/factura/componente/factura/factura.component';
import { SriComponent } from './Transaccional/sri/componente/sri/sri.component';
import { ActivaBuscarComponent } from './Menu/Boton/activa-buscar/activa-buscar.component';
import { GrabaTransaccionComponent } from './Menu/Boton/graba-transaccion/graba-transaccion.component';
import { CancelaTransaccionComponent } from './Menu/Boton/cancela-transaccion/cancela-transaccion.component';
import { BotoneraPrincipalComponent } from './Menu/Boton/botonera-principal/botonera-principal.component';
import { GestorErrorComponent } from './compartido/gestor-error/gestor-error.component';
import { TransaccionCabeceraComponent } from './Transaccional/transaccion-cabecera/transaccion-cabecera.component';
import { ModalComponent } from './Menu/modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PrincipalComponent,
    ErrorComponent,
    NoencontradaComponent,
    LateralComponent,
    CabeceraComponent,
    PeriodoComponent,
    UbicacionComponent,
    TabComponent,
    TabsComponent,
    FacturaComponent,
    SriComponent,
    ActivaBuscarComponent,
    GrabaTransaccionComponent,
    CancelaTransaccionComponent,
    BotoneraPrincipalComponent,
    GestorErrorComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RUTA_INICIAL,
    PrincipalModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    AutenticacionService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    }
  ],
  entryComponents: [TabComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
