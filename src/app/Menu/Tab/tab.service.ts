import { Injectable, OnInit } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { AccionMensaje } from '../../Infraestructura/Shell/Modelo/shell-modelo.indice';

@Injectable({
  providedIn: 'root'
})


export class TabService implements OnInit {
  
  //Envio de comandos (grabar,nuevo, editar ) a los componentes del TAB
  // private comandoSubject = new Subject<AccionMensaje>();
  // public comando$ = this.comandoSubject.asObservable();

  
  // private errorsSubject = new BehaviorSubject<string[]>([]);

  // errors$: Observable<string[]> = this.errorsSubject.asObservable();

  private comandoSubject = new BehaviorSubject<AccionMensaje>(new AccionMensaje());
  public comando$: Observable<AccionMensaje> = this.comandoSubject.asObservable();

  ngOnInit(){
   
  }

  /**** FUNCION: para avisar un botón presionado **/
  enviarComando(accion: AccionMensaje) {
    console.log(' *TabService => ', accion );
    this.comandoSubject.next(accion);   //Emite accionMensaje
  }


  constructor() { }
}
