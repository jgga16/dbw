import { EventEmitter, Component, OnInit, ViewChild,Input,Output, OnDestroy } from '@angular/core';
import { AccionMensaje, ConfiguracionComponente } from '../../Infraestructura/Shell/Modelo/shell-modelo.indice'
import { CoordinacionService } from '../../Infraestructura/Shell/Servicio/coordinacion.service';
import { Subscription, Observable } from 'rxjs';
import { RespuestaGrabarTransaccion, EstadoComponente, CommonErrorMessage } from '../../Infraestructura/Entidad/entidad.indice';
import { BotoneraPrincipalComponent } from '../Boton/botonera-principal/botonera-principal.component';


import { TabService } from './tab.service';
import { GestorErrorService } from '../../Infraestructura/Shell/Servicio/servicio.indice';
@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.css']

})
export class TabComponent implements  OnDestroy {

  public muestraError: boolean=false;
 
  @Input() idTab:number;
  @Input() tipoProceso:string;
  @Input() public proceso: any[]; //tiene la lista de componentes del proceso

  @Input() Titulo:string;
  @Input() estaActivo:boolean=false;
  @Input() plantilla;
  @Input() datoContexto;
  
  @Output() onClickContenido: EventEmitter<void> = new EventEmitter<void>();
  @Output() onBtnGrabar: EventEmitter<AccionMensaje> = new EventEmitter<AccionMensaje>();
  public refBotonera: Subscription = null;
  @ViewChild('botoneraPrincipal') botonera: BotoneraPrincipalComponent;

  //Ref Respuesta Transaccion
  public Respuesta$ = new  Observable<any>();
  public refRespuesta: Subscription = null;

  //Ref Errores de componente
  public refErrores: Subscription = null;

  //Errores del servidor + componetes
  public erroresGlobal: CommonErrorMessage[]

  constructor(public tabService: TabService, public coordinacionService: CoordinacionService, public gestorErrorSrv: GestorErrorService) { }

  // ngOnInit(){
  //   this.Respuesta$ = this.coordinacionService.Respuesta$.pipe(filter( (res) => res.IdAuxiliar=='3' ));      /// filter(res => res.endsWith(idTab.toString());
  //   // this.suscribirseBtnGrabar();
  //   // this.suscribirseRespuesta();
  // }

  ngOnDestroy() {
    this.refRespuesta.unsubscribe();
    this.refBotonera.unsubscribe();
  }

  seleccionaTab(){
    this.estaActivo= !this.estaActivo;
    this.onClickContenido.emit();
    alert("selecciono " + this.estaActivo);
  }

  //ENVIAR ACCION A COMPONETES
  onClick(){
    
    let accion: AccionMensaje = new AccionMensaje();
    accion.idTab = this.idTab;
    accion.accion ='Grabar'
    console.log(" Boton envia TAB =>",  this.idTab);
    this.onBtnGrabar.emit(accion);
  }

  listarErrores(errores: CommonErrorMessage[]) {
    this.muestraError = true;
    this.erroresGlobal = errores;
  }

  //ESCUCHA BOTONES


  // //ESCUCHA => RESPUESTA TRANSACCIONAL
  // suscribirseRespuesta():void{

  //   //aqui nulo porque ojo cambiar en CoordinacionService para que la notificacion devuelva una de tipo Respuesta 
  //   this.refRespuesta = this.coordinacionService.Respuesta$.subscribe(
  //     resp => {
  //         let Respuesta: RespuestaGrabarTransaccion = resp as RespuestaGrabarTransaccion;
  //          console.log(' Tab-Cmp => respuesta ' , Respuesta);
  //     },

  //     err => {
  //          console.log(' Tab-Cmp => error ' , err)

  //     }
  //   );

  // } //fin suscribirseRespuesta


  //ESCUCHA => RESPUESTA TRANSACCIONAL
  // suscribirseRespuesta():void{

  //   this.Respuesta$ = this.coordinacionService.Respuesta$.filter(res => res.IdTab == this.idTab);

  //   //aqui nulo porque ojo cambiar en CoordinacionService para que la notificacion devuelva una de tipo Respuesta 
  //   this.refRespuesta = this.Respuesta$.subscribe(
  //     resp => {
  //         let Respuesta: RespuestaGrabarTransaccion = resp as RespuestaGrabarTransaccion;
  //          console.log(' Tab-Cmp => respuesta ' , Respuesta);
  //     },

  //     err => {
  //          console.log(' Tab-Cmp => error ' , err)

  //     }
  //   );

  // } //fin suscribirseRespuesta



  // //ESCUCHA => BOTON
  // suscribirseBtnGrabar():void{
  //   //SUBSCRIBIRSE AL EVENTO ( OJO COMO SE BORRA LA REFERENCIA)
  //   this.refBtnGrabar= this.onBtnGrabar.subscribe(
  //     res => {
  //       console.log("Btn-Grabar => ",res);
       
  //       this.tabService.enviarComando((res as AccionMensaje));
  //     },
  //     err => {
  //       console.log("Btn-Grabar error")
  //     },
  //     () => {
  //       console.log("Btn-Grabar cierra instancia")
  //     }
  //   );
  // } 


  clickTabContenido(){
    this.onClickContenido.emit();
  }

}

