import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'btn-graba-transaccion',
  templateUrl: './graba-transaccion.component.html',
  styleUrls: ['./graba-transaccion.component.css']
})
export class GrabaTransaccionComponent implements OnInit {
  
  disabledGrabar:boolean=false;

  @Input() opcion:string;  
  @Output() onClick = new EventEmitter<string>();
 
  constructor() { }

  ngOnInit() {
  }


  onBotonClick() {
    if (this.opcion=='nuevo' )
      this.disabledGrabar=false;
    else
      this.disabledGrabar=true;

    alert('presiono boton => opcion ' + this.opcion );
    this.onClick.emit(this.opcion);
}


}
