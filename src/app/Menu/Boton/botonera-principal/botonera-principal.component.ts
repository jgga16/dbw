import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { AccionMensaje } from '../../../Infraestructura/Shell/Modelo/shell-modelo.indice';
import { Subscription, Observable } from 'rxjs';
import { RespuestaGrabarTransaccion } from '../../../Infraestructura/Entidad/entidad.indice';
import { CoordinacionService } from '../../../Infraestructura/Shell/Servicio/coordinacion.service';
import { filter } from 'rxjs/operators';


@Component({
  selector: 'botonera-principal',
  templateUrl: './botonera-principal.component.html',
  styleUrls: ['./botonera-principal.component.css']
})
export class BotoneraPrincipalComponent implements OnInit {
  
  @Input() IdTab:number;
  @Input() TipoProceso:string;

    _estadoComponente: string;
  get estadoComponente(): string {
      return this._estadoComponente;
  }

  @Input('estadoComponente')
  set estadoComponente(value: string) {
      this._estadoComponente = value;
      this.cambiaEstado(this._estadoComponente);
  }


  @Output() onBotonClick = new EventEmitter<AccionMensaje>();

  disabledError:boolean=false;

  disabledNuevo:boolean=false;
  disabledGrabar:boolean=true;
  disabledCancelar:boolean=true;


  disabledEditar:boolean=true;
  disabledReprocesar:boolean=true;
  disabledReverso1:boolean=true;
  disabledReverso2:boolean=true;
  disabledReverso3:boolean=true;
  disabledCopiar:boolean=true;

  disabledReversarOps:boolean=true;
  disabledPendienteFe:boolean=true;

  disabledTimeLine:boolean=true;
  disabledTracking:boolean=true;
  
  disabledGenerarReporte:boolean=true;
  disabledImprimir:boolean=true;
  
  disabledPersonalizado01:boolean=true;
  disabledPersonalizado02:boolean=true;
  disabledPersonalizado03:boolean=true;
  disabledPersonalizado04:boolean=true;
  disabledPersonalizado05:boolean=true;
  disabledPersonalizado06:boolean=true;
  disabledPersonalizado07:boolean=true;
  disabledPersonalizado08:boolean=true;
  disabledPersonalizado09:boolean=true;
  disabledPersonalizado10:boolean=true;

  //SUBSCRIPCIN COORDINADOR
  public refRespuesta: Subscription;
  public Respuesta$: Observable<RespuestaGrabarTransaccion>;


  constructor(public coordinacionSrv: CoordinacionService) { }

  ngOnInit() {
  }

  onClickBoton(opcion) {
    //CAMBIOAR ESTADO BOTONES
    this.cambiaEstado(opcion);

    //ENVIAR MENSAJE AL TAB
    let accion: AccionMensaje = new AccionMensaje(); 
    accion.accion=opcion;
    accion.idTab=this.IdTab; 
    accion.tipoProceso=this.TipoProceso;
    this.onBotonClick.emit(accion);
  }

  // subscribirseRespuesta():void {

  //      this.Respuesta$ = this.coordinacionSrv.Respuesta$.pipe(filter( (res) => res.IdTab== this.IdTab ));      /// filter(res => res.endsWith(idTab.toString());

  //      this.refRespuesta = this.Respuesta$.subscribe(
  //        resp => {
  //            let respuestaTransaccion: RespuestaGrabarTransaccion = resp as RespuestaGrabarTransaccion;
  //            if (this.IdTab!=respuestaTransaccion.IdTab) return 
  //        },
   
  //        err => {
  //             alert('Botonera => error ' + err);
  //             this.cambiaEstado('nuevo');  
   
  //        }
  //      );   
  // }

  cambiaEstado(estado:string):boolean {

    if(estado=='default') {
      this.disabledNuevo=false;
      this.disabledGrabar=true;
      this.disabledCancelar=true;
      return true;
    }
      
    //NUEVO
    if(estado=='nuevo') {
      this.disabledNuevo=true;
      this.disabledGrabar=false;
      this.disabledCancelar=false;
      return true;
    }

    //GRABADO
    if(estado=='grabado') {
      this.disabledGrabar=true;
      this.disabledCancelar=true;
      this.disabledNuevo=false;
      return true;
    }



    //CANCELAR
    if(estado=='cancelar') {
      this.disabledNuevo=false;
      this.disabledGrabar=true;
      this.disabledCancelar=true;
      return true;
    }




    if(estado=='nuevo' && this.TipoProceso=="RE") {
      this.disabledGrabar=false;
      return true;
    }




    if(estado=='cancelar' && this.TipoProceso=="RE") {
      this.disabledNuevo = false;
      this.disabledCancelar = true;
      return true;
    }




    return false;
  }

}
