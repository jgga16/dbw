import { Component, OnInit, AfterContentInit, ContentChildren, QueryList, Input, ViewContainerRef, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { ITab } from '../Tab/itab.interface';
import { TabComponent } from '../tab/tab.component-new';
import { TabsDinamicoDirective } from './tabs-dinamico.directive';
import { TabService } from '../tab/tab.service';
import { AccionMensaje } from '../../Infraestructura/Shell/Modelo/accion-mensaje';
import { CoordinacionService } from '../../Infraestructura/Shell/Servicio/coordinacion.service';
import { filter } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { RespuestaGrabarTransaccion, EstadoComponente } from '../../Infraestructura/Entidad/entidad.indice';
import { ComponenteErrorMessage } from '../../Infraestructura/Entidad/respuesta-grabar-transaccion';


@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements OnInit, AfterContentInit {


  public dinamicoTabs: TabComponent[] = [];

  @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;
  @ViewChild(TabsDinamicoDirective) dinamicoTabContenedor: TabsDinamicoDirective;
  @ViewChild('contenedor', {read: ViewContainerRef}) contenedorTabRef;

  @Input() public lstTabs: ITab[]= [];



  constructor(private _resolver: ComponentFactoryResolver ,public tabService: TabService, public coordinacionSrv: CoordinacionService) { 
  }

  abrirTab(proceso: EstadoComponente[], tipoProceso:string,titulo:string, plantilla:any, estadoComponente: EstadoComponente) {

    let componentFactory = this._resolver.resolveComponentFactory(TabComponent);
    let componentRef = this.contenedorTabRef.createComponent(componentFactory);
    let instance: TabComponent = componentRef.instance as TabComponent;
    let idTab: number;    
    idTab =  this.dinamicoTabs.length + 1;

    //Datos de Instancia del Tab
    instance.idTab = idTab;
    instance.tipoProceso = tipoProceso;
    instance.proceso = proceso;

    //Datos de isntacion del componente del Tab
    estadoComponente.idTab =  idTab; 
    
    //Datos de contexto del template y sus componentes
    instance.datoContexto = {idTab: idTab, estadoComponente: estadoComponente } ;

    //console.log("Asignando a la instancia ",  instance.proceso);

    //BOTONERA
    instance.refBotonera = instance.botonera.onBotonClick.subscribe(
      res => {
        //console.log("Tab.botonera => ",res);
       
        this.tabService.enviarComando((res as AccionMensaje));
      },
      err => {
        console.log("Tab.botonera error")
      },
      () => {
        console.log("botonera cierra instancia")
      }
    );
    
    //subscribe la botonera => respuestaTransaccion en servicion de coordinacion
    instance.botonera.Respuesta$ = instance.botonera.coordinacionSrv.Respuesta$;
    instance.botonera.refRespuesta = instance.botonera.Respuesta$.subscribe(
      resp => {
          let respuestaTransaccion: RespuestaGrabarTransaccion = resp as RespuestaGrabarTransaccion;
          debugger;
          //CAMBIAR ESTADO  
          if (respuestaTransaccion.Errores.length>0) {
             console.log("    **  Botonera-Cmp ==============> estado error");
             instance.botonera.estadoComponente = 'nuevo';
             instance.listarErrores(respuestaTransaccion.Errores);

           }   
          else {
           console.log("    **  Botonera-Cmp ==============> estado exitosa");
           instance.botonera.estadoComponente = 'default';
          }
           

      },

      err => {
           alert('Botonera => error ' + err);
          //  instance.cambiaEstado('nuevo');  

      }
    ); 



    //RECEPCION DE ERRORES
    instance.refErrores= instance.gestorErrorSrv.Errores$.subscribe(  
      res => {
        let existeErrorProceso:boolean; // Errores del proceso dibujado en el Tab
        let errorProceso:ComponenteErrorMessage[];
        errorProceso = res; // Tomar la respuesta
        existeErrorProceso = errorProceso.some(err => err.EstadoComponente.idTab === instance.idTab)
        if (existeErrorProceso) return 
        console.log('       Tab => GestorError-Srv => erroresProceso' , res);
      },
      err => {
        console.log('       Tab => GestorError-Srv => error' , err);
      },
      () => {
        console.log("       Tab => GestorError-Srv => completo")
      }
    );





    //RESPUESTA TRANSACCION
    instance.Respuesta$ = instance.coordinacionService.Respuesta$; //.pipe(filter( (res) => (res.IdTab == instance.idTab) )) ;

    instance.refRespuesta= instance.Respuesta$.subscribe(  //this.coordinacionSrv.Respuesta$
      res => {
        if (instance.idTab!=res.IdTab) return 
        // console.log("       TAB INSTANCIA => " + instance.idTab + " RESPUESTA " + res.IdTab);
        //console.log('       Tab => Coordinacion-Srv => respuesta' , res);
   
      },
      err => {
        console.log('       Tab => Coordinacion-Srv => error' , err);
      },
      () => {
        console.log("       Tab => Coordinacion-Srv => completo")
      }
    );

 //COLOCAR DATOS INSTANCIA
//  let id =  this.dinamicoTabs.length + 1;

 instance.Titulo = titulo + instance.idTab;
 instance.plantilla = plantilla;

 // instance.template = template;
 // instance.datoContexto = {idTab:instance.idTab } ;

 alert("Creacion dinamica " + titulo);
 

    this.dinamicoTabs.push(componentRef.instance as TabComponent);

    // set it active
    this.seleccionaTab(this.dinamicoTabs[this.dinamicoTabs.length - 1]);

  }


  cerrarTab(tab){
    for (let i = 0; i < this.dinamicoTabs.length; i++) {
      if (this.dinamicoTabs[i] === tab) {
        // remove the tab from our array
        this.dinamicoTabs.splice(i, 1);

        // destroy our dynamically created component again
        let viewContainerRef = this.contenedorTabRef;

        if (tab.datoContexto.IdAmbiente) {
          alert("Borrar el storage");
        }
        
        console.log("Contenedor Referencia => ",viewContainerRef);

        //borrar
        viewContainerRef.remove(i);
        
        // set tab index to 1st one
        this.seleccionaTab(this.dinamicoTabs[this.dinamicoTabs.length - 1]);
        break;
      }
    }
  }

  seleccionaTab(tab:TabComponent) {
 
     this.dinamicoTabs.forEach(tab => (tab.estaActivo = false));
    //  tab.estaActivo=true;
     // alert("Selecciono => " + tab.Titulo + " " + tab.estaActivo);
  }

  ngOnInit(){


    //this.tabs.toArray().push({estaActivo:false,titulo:"Ficha-1"})
  }
 // contentChildren are set

 ngAfterContentInit() {
  // get all active tabs
  const activeTabs = this.tabs.filter(tab => tab.estaActivo);

  // if there is no active tab set, activate the first
  if (activeTabs.length === 0) {
    this.seleccionaTab(this.tabs.first);
  }
}

 


}
