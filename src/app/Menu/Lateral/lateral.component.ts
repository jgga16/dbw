import { Component, OnInit, Output, Input } from '@angular/core';
import { DbWareMenuApi } from '../../Infraestructura/Entidad/menu-api';
import { EventEmitter } from '@angular/core';
import { LateralService } from '../Servicio/lateral.service';

@Component({
  selector: 'app-lateral',
  templateUrl: './lateral.component.html',
  styleUrls: ['./lateral.component.css']
})
export class LateralComponent implements OnInit {

  // Controls the display of a minimized or full menu
  // menuFullDisplay: Boolean;
  // Contains all the main menu parts
  menuParts: Array<DbWareMenuApi>;
  // Reference of the currently active submenu
  activeInstance: number = 0;

  // Dummy arrays used for testing
  levelOne: Array<DbWareMenuApi>;
  levelTwo: Array<DbWareMenuApi>;
  levelThree: Array<DbWareMenuApi>;

  @Input() menuFullDisplay: Boolean = false;
  @Output() notifyMenu: EventEmitter<DbWareMenuApi> = new EventEmitter<DbWareMenuApi>();


  constructor(public lateralService: LateralService) { }

  ngOnInit() {
    this.levelThree = this.lateralService.levelThree;
    this.levelTwo = this.lateralService.levelTwo;
    this.levelOne = this.lateralService.levelOne;
    this.menuParts = this.lateralService.menuParts;
  }

  // function which is executed by the controller button of the menu which
  // toggles between full or minimized menu modes
  buttonToggleDisplay() {
    this.menuFullDisplay = !this.menuFullDisplay;
    // this.closeAll();
  }


  chosenMenu(selected: DbWareMenuApi) {
    if (selected.Hijos) {
      this.toggleShown(selected);
    }
    else {
      this.notifyMenu.emit(selected);
    }
  }

  onSubmit(form: any) {
    if (form.search.length < 3) {
      this.closeAll();
    }
    else if (form.search.length > 2) {
      this.recursiveLookup(this.menuParts, form.search);
    }
  }

  // to be used by the reset button to revert menu to original functionality 
  // closing all the matched menu elements and setting them all as enabled again
  closeAll() {
    for (let i = 0; i < this.menuParts.length; i++) {
      this.closeKids(this.menuParts[i]);
    }
  }

  recursiveLookup(subMenu: Array<DbWareMenuApi>, query: String): boolean {
    let resonateBack = false;
    for (let i = 0; i < subMenu.length; i++) {
      let matchFound = false;
      const element = subMenu[i];

      if (element.Hijos) { //start by going to the leaves and finding matches there
        matchFound = this.recursiveLookup(element.Hijos, query);
      }

      if (element.Nombre.toLowerCase().indexOf(query.toLowerCase().toString()) != -1) {
        matchFound = true; //if this has a match it doesnt get overwritten by leaves
        element.Disable = false;
      }
      else {
        element.Disable = true; //disable all the unmatching options, even if they are hidden
      }
      element.IsShown = matchFound; //assign shown bot not enable in case only a leaf matches
      if (matchFound) {
        resonateBack = true; //all its ancestors need to be shown to show any matched leaf
      }
    }
    return resonateBack;
  }



  // function exclusive of the fist level of menu components
  // collapses the selected submenu, sets it as the active
  // submenu and closes the previous one
  revealMain(indexSelected: number) {
    // always trigger full menu 
    this.menuFullDisplay = true;
    if (this.activeInstance === indexSelected) {
      this.menuParts[this.activeInstance].IsShown = !this.menuParts[this.activeInstance].IsShown;
      if (!this.menuParts[this.activeInstance].IsShown) {
        this.closeKids(this.menuParts[this.activeInstance]);
      }
    }
    else {
      this.menuParts[this.activeInstance].IsShown = false;//close current active
      this.closeKids(this.menuParts[this.activeInstance]);
      this.activeInstance = indexSelected;//set new active
      this.menuParts[indexSelected].IsShown = true;//open new active
    }
  }

  closeKids(menu: DbWareMenuApi) {
    console.log("closed");
    if (menu.Hijos === null) {
      menu.IsShown = false;
      menu.Disable = false;
      return;
    }
    let i = 0;
    for (i; i < menu.Hijos.length; ++i) {
      this.closeKids(menu.Hijos[i]);
      menu.IsShown = false;
      menu.Disable = false;
    }
  }

  // method that, given a child of this object, either
  // toggles it if the item has children of its own
  toggleShown(menu: DbWareMenuApi) {
    menu.IsShown = !menu.IsShown;
  }

  // method that controls a Disabled class for when the
  // menu is being unmatched by the search option
  toggleDisabled(menu: DbWareMenuApi) {
    menu.Disable = !menu.Disable;
  }

}

