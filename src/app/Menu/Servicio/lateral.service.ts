import { Injectable } from '@angular/core';
import { DbWareMenuApi } from '../../Infraestructura/Entidad/menu-api';
import { GestorCacheService } from '../../Compartido/GestorCache/gestor-cache.service';


@Injectable({
  providedIn: 'root'
})
export class LateralService {

  public levelThree = [
    new DbWareMenuApi(null, null, ".png", "I haaaaaaaate scroll bars", null),
  ]
  public levelTwo = [
    new DbWareMenuApi(null, null, ".png", "Grandchild - 1", null),
    new DbWareMenuApi(null, null, ".png", "Grandchild - 2", this.levelThree),
  ]
  public levelOne = [
    new DbWareMenuApi(null, null, ".png", "Child - 1", this.levelTwo),
    new DbWareMenuApi(null, null, ".png", "Child - 2", this.levelTwo),
    new DbWareMenuApi(null, null, ".png", "Child - 3", this.levelTwo)
  ]
  public menuParts = [
    new DbWareMenuApi(null, null, "icon-home", "Option1", this.levelOne),
    new DbWareMenuApi(null, null, "icon-images", "Option2", this.levelOne),
    new DbWareMenuApi(null, null, "icon-books", "Option3", this.levelOne),
    new DbWareMenuApi(null, null, "icon-compass2", "Option4", this.levelOne),
    new DbWareMenuApi(null, null, "icon-rocket", "Option5", this.levelOne),
    new DbWareMenuApi(null, null, "icon-link", "Option6", this.levelOne)
  ]

  constructor(public gestorCache: GestorCacheService) {

    this.menuParts = [];
    this.levelOne = [];
    for (let menu of this.gestorCache.MenuApi) {
      // console.log("menu => ", menu.Nombre );
      if (menu.Hijos) {
        var nivelUno: Array<DbWareMenuApi> = [];
        for (let hijo of menu.Hijos) {
          // console.log(" * menuHijo => ", hijo.Nombre );
          if (hijo.Hijos) {
            var nivelDos: Array<DbWareMenuApi> = [];
            for (let nieto of hijo.Hijos) {
              nieto.Icon = "compass2"; //boorar
              nivelDos.push(new DbWareMenuApi(null, null, nieto.Icon, nieto.Nombre, null));
              //console.log(" ** nieto => ", nieto.Nombre );
            }
            hijo.Icon = "icon-books"; //boorar
            nivelUno.push(new DbWareMenuApi(null, null, hijo.Icon, hijo.Nombre, nivelDos))
          } else {
            hijo.Icon = "icon-compass2"; //boorar
            nivelUno.push(new DbWareMenuApi(null, null, hijo.Icon, hijo.Nombre, null))
          }
        }
        menu.Icon = "icon-home"; //boorar
        this.menuParts.push(new DbWareMenuApi(null, null, menu.Icon, menu.Nombre, nivelUno));
      }
      else {
        this.menuParts.push(new DbWareMenuApi(null, null, menu.Icon, menu.Nombre, null));
      }
    }


  }


}
