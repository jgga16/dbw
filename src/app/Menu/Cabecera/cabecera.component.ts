import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { asEnumerable } from 'linq-es5';

import { DbWareMenuApi } from '../../Infraestructura/Entidad/menu-api';
import { ICatalogoAnio } from '../Modelo/ICatalogoAnio';
import { ICatalogo } from '../Modelo/ICatalogo';
import { IdCollection } from '../Modelo/IdCollection';

@Component({
  selector: 'app-cabecera',
  templateUrl: './cabecera.component.html',
  styleUrls: [
    './cabecera.component.css',
    './cabecera-animation.component.css'
  ]
})

export class CabeceraComponent implements OnInit {

  // constant defining number of boxes in first dropdown
  private readonly MAINNUM: number = 6;
  // constant defining number of boxes in children menus
  private readonly CHILDNUM: number = 8;
  // default id values from last session
  @Input() savedIds: IdCollection

  // data values to populate selects
  @Input() CompanyList: Array<ICatalogo>
  @Input() DepartmentList: Array<ICatalogo>
  @Input() accountingIntervals: Array<ICatalogoAnio>
  @Input() payrollIntervals: Array<ICatalogoAnio>
  // Dyamic menu data array
  @Input() menuParts: Array<DbWareMenuApi>;

  //determines which of the panels dropdowns is shown
  displayPanel: Number
  // Reference of the currently active main submenu
  activeInstances: Array<number> = new Array<number>(4);

  //Variables that store the select FormControls and options
  mainForm: FormGroup
  selCompanies: Array<String>
  selDepartments: Array<String>
  selAccountingY: Array<String>
  selAccountingP: Array<String>
  selPayrollY: Array<String>
  selPayrollP: Array<String>

  constructor(fb: FormBuilder) {
    this.mainForm = fb.group({
      'company': ['NoComp'],
      'department': ['NoDept'],
      'accYear': ['Year'],
      'accPeriod': ['Period'],
      'payYear': ['Year'],
      'payPeriod': ['Period']
    })
    this.activeInstances.fill(0);
  }

  ngOnInit() {
    if (this.CompanyList) {
      let savedCompany = asEnumerable(this.CompanyList).Where(p => p.id == this.savedIds.idCompania).SingleOrDefault();

      this.mainForm.get('company').setValue(savedCompany.nombre);
      this.selCompanies = asEnumerable(this.CompanyList).Select(p => p.nombre).Distinct().ToArray();
    }
    if (this.DepartmentList) {
      let savedDepartment = asEnumerable(this.DepartmentList).Where(p => p.id == this.savedIds.idDepartamento).SingleOrDefault();
      this.mainForm.get('department').setValue(savedDepartment.nombre);
      this.selDepartments = asEnumerable(this.DepartmentList).Select(p => p.nombre).Distinct().ToArray();
    }
    if (this.accountingIntervals) {
      let savedAccInterval = this.lookForDefault(this.savedIds.idPeriodo, this.accountingIntervals);
      this.mainForm.get('accYear').setValue(savedAccInterval.anio);
      this.mainForm.get('accPeriod').setValue(savedAccInterval.nombre);
      this.selAccountingY = asEnumerable(this.accountingIntervals).Select(p => p.anio).Distinct().ToArray();
      this.selAccountingP = asEnumerable(this.accountingIntervals).Where(p => p.anio == this.mainForm.value.accYear).Select(p => p.nombre).ToArray();
    }
    if (this.payrollIntervals) {
      let savedPayInterval = this.lookForDefault(this.savedIds.idNominaPeriodo, this.payrollIntervals);
      this.mainForm.get('payYear').setValue(savedPayInterval.anio);
      this.mainForm.get('payPeriod').setValue(savedPayInterval.nombre);
      this.selPayrollY = asEnumerable(this.payrollIntervals).Select(p => p.anio).Distinct().ToArray();
      this.selPayrollP = asEnumerable(this.payrollIntervals).Where(p => p.anio == this.mainForm.value.payYear).Select(p => p.nombre).ToArray();
    }
  }

  onSubmit(form: any): void {
    console.log('you submitted value:', form);
  }

  // from the year given by the user, refresh period list to reflect periods containing the year
  choosePeriod(chosenYear: String, Intervals: Array<ICatalogoAnio>, form: AbstractControl): Array<String> {
    let per = asEnumerable(Intervals).Where(p => p.anio == chosenYear).Select(p => p.nombre).ToArray();
    if (!asEnumerable(per).Contains(form.value)) {
      form.setValue("?");
    }
    return per;
  }

  // To be sent to the parent component, the ID of the interval the user desires to see
  submitIdInterval(chosenYear: String, chosenPeriod: String, Intervals: Array<ICatalogoAnio>): String {
    let id = asEnumerable(Intervals).Where(p => { return p.anio == chosenYear && p.nombre == chosenPeriod }).Select(p => p.id).SingleOrDefault();
    if (id) {
      console.log('Show User Interval:', id);
      return id;
    }
    else {
      console.log('none found:', id);
      return null;
    }
  }

  // Used to look for the selections the user had open last session and return the interval corresponding to the id
  lookForDefault(recordedId: string, Intervals: Array<ICatalogoAnio>): ICatalogoAnio {
    return asEnumerable(Intervals).Where(p => p.id == recordedId).SingleOrDefault();
  }

  // Function that controls the css which show the selected menu and closes the others
  panelToggle(p: number) {
    if (this.displayPanel == p) {
      this.displayPanel = null;
    }
    else {
      this.displayPanel = p;
    }
  }

  // Macro function to open and close any menu, including closing its children
  revealMenu(atLevel: number, indexSelected: number, parent: Array<DbWareMenuApi>, ) {
    let atIndex = this.activeInstances[atLevel];
    // if the menu that was selected is the active one:
    if (atIndex == indexSelected) {
      // Emit it multiple times if it is a leaf
      if (!parent[atIndex].Hijos || parent[atIndex].Hijos.length < 1 ) {
        // If option, close menu
        this.displayPanel = null;
        console.log("emit!");
        return;
      }
      // Else hide it, close the kids and set index to null
      else {
        parent[atIndex].IsShown = false
        this.closeKids(parent[atIndex]);
        this.activeInstances.fill(null, atLevel); //nullify the levels after this one
      }
    }
    //else close active, open selected and change active instance at that level
    else {
      if (atIndex != null) {
        parent[atIndex].IsShown = false;//close current active
        this.closeKids(parent[atIndex]);
      }
      this.activeInstances.fill(null, atLevel); //nullify the levels after this one
      this.activeInstances[atLevel] = indexSelected;//set new active
      if (parent[indexSelected].Hijos) {
        parent[indexSelected].IsShown = true;//open new active if has submenus
      }
      else if (!parent[indexSelected].Hijos || parent[atIndex].Hijos.length < 1 ) {
        // If option, close menu
        this.displayPanel = null;
        // else emit object to parent
        console.log("emit!");
      }
    }
  }

  // Returns true or false if it is moment to place the row separator and create a new row
  setPartition(index: number, item: DbWareMenuApi): boolean {
    const size = Math.min(item.Hijos.length, this.CHILDNUM * 2);
    return (index == Math.floor(size / 2)) && (size > this.CHILDNUM);
  }

  // Returns true or false if item is overflowing the two allowed rows and shouldnt be shown
  isChildFull(i: number, item: DbWareMenuApi): boolean {
    let index = i;
    // If null it means overflow menu is calling
    if (index === null) {
      index = this.CHILDNUM * 2;
    }
    // overflow if index surpasses the max of 2 rows
    return item.Hijos.length > this.CHILDNUM * 2 && index > this.CHILDNUM * 2 - 2;
  }


  // Similar to isChildFull but for the first submenu, uses a different constant
  isMainFull(i: number, children: Array<DbWareMenuApi>): boolean {
    if (!children)
      return false;
    let index = i;
    if (index === null) {
      index = this.MAINNUM;
    }
    return children.length > this.MAINNUM && index > this.MAINNUM - 2;
  }

  signOutUser() {
    // TODO make mathod log out the user and maybe redirect
    console.log("Boom! Someone will log you out soon")
  }

  closeKids(menu:DbWareMenuApi) {
    console.log("closed");
    if (menu.Hijos === null) {
        menu.IsShown = false;
        menu.Disable = false;
        return;
    }
    let i = 0;
    for (i; i < menu.Hijos.length; ++i) {
        this.closeKids(menu.Hijos[i]);
        menu.IsShown = false;
        menu.Disable = false;
    }
}

  // method that, given a child of this object, either
  // toggles it if the item has children of its own
  toggleShown(menu: DbWareMenuApi) {
    menu.IsShown = !menu.IsShown;
  }

  // method that controls a Disabled class for when the
  // menu is being unmatched by the search option
  toggleDisabled(menu: DbWareMenuApi) {
    menu.Disable = !menu.Disable;
  }

  // William Stuff
  @Output() onConmutaLateral: EventEmitter<boolean> = new EventEmitter<boolean>();
  conmutarLateral(): void {
    this.onConmutaLateral.emit(true);
  }
}
