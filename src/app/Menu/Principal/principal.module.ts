import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DepartamentoComponent } from '../../Administrativo/Departamento/Pagina/departamento/departamento.component';
import { SucursalComponent } from '../../Administrativo/Departamento/Pagina/sucursal/sucursal.component';
import { RUTAS_PRINCIPAL } from './principal.routes';


@NgModule({
  declarations: [
    DepartamentoComponent,
    SucursalComponent
  ],
  exports: [
    DepartamentoComponent,
    SucursalComponent 
  ],
  imports: [
    CommonModule,
    RUTAS_PRINCIPAL
  ]
})
export class PrincipalModule { }
