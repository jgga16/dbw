import { Component, OnInit, OnDestroy, ContentChildren, QueryList, AfterViewInit, ComponentFactoryResolver, ViewChild, ViewContainerRef, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { PreferenciaService, CoordinacionService } from '../../../Infraestructura/Shell/Servicio/servicio.indice';
import {  AmbientePeriodo } from '../../../Infraestructura/Shell/Modelo/ambiente-periodo';
import { AmbienteUbicacion } from '../../../Infraestructura/Shell/Modelo/ambiente-ubicacion';
import { AccionMensaje } from '../../../Infraestructura/Shell/Modelo/accion-mensaje';
import { EstadoComponente } from '../../../Infraestructura/Entidad/estadocomponente';
import { CONSTANTE } from '../../../Infraestructura/Constante/constante';
import { ITab } from '../../Tab/itab.interface';
import { TabsComponent } from '../../tabs/tabs.component';
import { DbWareMenuApi } from '../../../Infraestructura/Entidad/menu-api';
import { GestorCacheService } from '../../../Compartido/GestorCache/gestor-cache.service';


@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit, OnDestroy {

  public mostrar: boolean;
  public ListaControles: Array<number> = new Array<number>();
  public DiccionarioControles: { [id: string]: boolean; } = {};
  private menuParts:Array<DbWareMenuApi>

  @ViewChild('contenedor', { read: ViewContainerRef }) contenedorTabRef;
  @ViewChild(TabsComponent) tabsComponent;
  @ViewChild('plantillaUno') plantillaUno;
  @ViewChild('plantillaDos') plantillaDos;

  constructor(public cacheService: GestorCacheService,    public preferenciaService: PreferenciaService, private _resolver: ComponentFactoryResolver,private coordinacionSrv: CoordinacionService) {  }
  
  crearTab(NroPlantilla:number, tipoProceso:string) {

    let estadoComponenteUno: EstadoComponente = new EstadoComponente();
    let estadoComponenteDos: EstadoComponente= new EstadoComponente();
    let estadoComponenteTres: EstadoComponente= new EstadoComponente();


    estadoComponenteUno.idTab = CONSTANTE.ID_TAB_NO_DEFINIDO;
    estadoComponenteUno.idComponente = '123';
    estadoComponenteUno.orden=2;
    estadoComponenteUno.estado= 'Inicializado';

    estadoComponenteDos.idTab = CONSTANTE.ID_TAB_NO_DEFINIDO;
    estadoComponenteDos.idComponente = 'abc';
    estadoComponenteDos.orden=3;
    estadoComponenteDos.estado= 'Inicializado';



    estadoComponenteTres.idTab =  CONSTANTE.ID_TAB_NO_DEFINIDO;
    estadoComponenteTres.idComponente = 'xyz';
    estadoComponenteTres.orden=1;
    estadoComponenteTres.estado= 'Inicializado';



    //Un proceso es un arreglo de componentes
    var proceso: EstadoComponente[]=[];
   
    proceso[CONSTANTE.ID_CMP_FACTURA]= estadoComponenteUno;
    proceso[CONSTANTE.ID_CMP_SRI]= estadoComponenteDos;
    proceso[CONSTANTE.ID_CMP_TRSC]= estadoComponenteTres;
   
    //Almacena el estado (entregado/no entregado) al coordinardor.
    this.coordinacionSrv.creaProcesoEstado('ProcesoUno',proceso);


    let datoContexto: EstadoComponente;

    if (NroPlantilla == 1) {
      this.tabsComponent.abrirTab(proceso, tipoProceso, "TAB-" + NroPlantilla, this.plantillaUno, estadoComponenteUno);
    }

    if (NroPlantilla == 2) {
      this.tabsComponent.abrirTab(proceso, tipoProceso, "TAB-" + NroPlantilla, this.plantillaDos, estadoComponenteDos);
    }
  }

  ngOnInit() {
    this.ListaControles.push(1);
    this.DiccionarioControles["compania"] = false;
    this.DiccionarioControles["periodo"] = false;
    this.DiccionarioControles["ubicacion"] = false;
    this.menuParts = this.cacheService.MenuApi;
  }

  ngOnDestroy() {

  }

  conmutar($event) {
    //Colapasar menu-lateral
    this.mostrar = !this.mostrar;
  }

  crearControlOld() {
    var siguiente = this.ListaControles.length + 1
    this.ListaControles.push(siguiente);
  }

  crearControl() {
    this.DiccionarioControles["compania"] = true;
    this.DiccionarioControles["periodo"] = true;
    this.DiccionarioControles["ubicacion"] = true;
  }

  recuperaPeriodo(Id: string) {
    console.log("Id => ", Id);
    var AmbientePeriodo: AmbientePeriodo = this.preferenciaService.obtiene('Compania', Id);
    console.log("Ambiente => ", AmbientePeriodo);
    alert(" Anio: " + AmbientePeriodo.Anio + " Mes: " + AmbientePeriodo.Mes);
  }

  recuperaUbicacion(Id: string) {
    var AmbienteUbicacion: AmbienteUbicacion = this.preferenciaService.obtiene('Grupo', Id);
    alert(" Compania: " + AmbienteUbicacion.Compania + " Departamento: " + AmbienteUbicacion.Departamento);
  }

}
