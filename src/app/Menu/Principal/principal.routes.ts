import { Routes, RouterModule } from "@angular/router";
import { PrincipalComponent } from "./principal/principal.component";
import { SucursalComponent } from "../../Administrativo/Departamento/Pagina/sucursal/sucursal.component";
import { DepartamentoComponent } from "../../Administrativo/Departamento/Pagina/departamento/departamento.component";



//RUTAS 
const principalRutas: Routes = [
    {path:'', component: PrincipalComponent,
     children: [
        {path:'sucursal', component: SucursalComponent},
        {path:'departamento', component: DepartamentoComponent},
        {path:'', redirectTo: '/principal', pathMatch: 'full' },
     ]
}];
   

export const RUTAS_PRINCIPAL = RouterModule.forChild(principalRutas);

