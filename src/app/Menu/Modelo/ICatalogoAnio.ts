// Interface to im port period intervals into select forms
export interface ICatalogoAnio {
    id: string;
    anio: string;
    nombre: string;
    numero: number
    codigo:string
}