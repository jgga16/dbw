// Class to be used to pass strings representing the form values of the last user session
export class IdCollection {
    user:string
    role:string
    idCompania: string
    idDepartamento: string
    idPeriodo: string
    idNominaPeriodo: string

    constructor(givenu:string, givenr:string, comp: string, dep: string, acc: string, pay: string) {
        this.user = givenu;
        this.role = givenr;
        this.idCompania = comp;
        this.idDepartamento = dep;
        this.idPeriodo = acc;
        this.idNominaPeriodo = pay;
    }
}