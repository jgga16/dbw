// Interface to import companies and departments into the select forms
export interface ICatalogo {
    id: string;
    nombre: string;
    codigo:string
}