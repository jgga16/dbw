import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ModalRespuesta } from './modal-respuesta';
import { PeticionLeer, RespuestaLeer, Criterios } from '../../Infraestructura/Entidad/entidad.indice';
import { NgForm } from '@angular/forms';
import { CatalogoService } from '../../Infraestructura/Shell/Servicio/servicio.indice';
import { Subscription } from '../../../../node_modules/rxjs';
import { GestorCacheService } from '../../Compartido/GestorCache/gestor-cache.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  //Parametros de busquedad
  public codigo:string;
  public nombre:string;

  //Subcipcion para recibir del servicio de catalogo
  public refRespuesta: Subscription = null;
  public Respuesta: RespuestaLeer;

  //Respuesta del usuario
  private modalRespuesta: ModalRespuesta = new ModalRespuesta();
  @Input() catalogo:string;
  @Input() titulo:string;
  @Output() respuestaModal: EventEmitter<ModalRespuesta> = new EventEmitter();
  
  constructor(public catalogoSrv: CatalogoService, public gestorCache: GestorCacheService) { }

  ngOnInit() {
   
  }

  aceptar() {
    this.modalRespuesta.nombreBoton='aceptar'
    this.modalRespuesta.idClave='id_aceptar';
    this.respuestaModal.emit(this.modalRespuesta);
    console.log('Presiono aceptar');
  }

  cancelar() {
    this.modalRespuesta.nombreBoton='cancelar'
    this.modalRespuesta.idClave='';
    this.respuestaModal.emit(this.modalRespuesta);
    console.log('Presiono cancelar');
  }

  buscar() {
    this.modalRespuesta.nombreBoton='buscar'
    this.modalRespuesta.idClave='id_busca';
    this.enviarPeticion();
    console.log('Presiono buscar');
  }

  enviarPeticion(){
    let peticionLeer: PeticionLeer = new PeticionLeer();
    let criterio: Criterios = new Criterios();
    
    //Criterio de busquedad
    criterio.Campo = "idCompania";
    criterio.Operador= OperadorBusqueda.Igual;
    criterio.Tipo = TipoDatoCriterio.Texto;
    criterio.Valor = "C37E7C16-52DA-4E58-A80F-3842E880394A"
    peticionLeer.Criterios=[];
    peticionLeer.Criterios.push(criterio);

    //Peticion
    peticionLeer.Hash = this.gestorCache.SesionHash;
    peticionLeer.TipoCatalogo = CatalogosType.Bodega;
    peticionLeer.SortOrder = SortOrder.Ascending;
    
    this.catalogoSrv.leerCatalogoPaginado(peticionLeer);
    this.suscribirseRespuesta();
    
  }

  suscribirseRespuesta():void {
    this.refRespuesta = this.catalogoSrv.Respuesta$.subscribe(
      res => this.resRespuesta(res),
      err => this.errRespuesta(err),
      () => this.comRespuesta()
    );
  }
  
    resRespuesta(res) {
  
        //this.Respuesta = (JSON.parse(res) as RespuestaLeer); 
        this.Respuesta = res;
  
        //MOSTRAR EN GRILLA
        console.log('        *** ModalComponent => RespuestaLeer ' , this.Respuesta);
    }
  
    errRespuesta(err) {
        console.log("ModalComponent: no se obtuvo respuesta del servicio de datos");
    }
  
    comRespuesta() {
        console.log("ModalComponent: el flujo se completo");
  
    }


}
