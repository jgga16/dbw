import { 
    Compania,Categoria,UsuarioCompania,Periodo, Flujo, NominaPeriodo,Bodega,
    CentroCosto,Departamento,Sucursal,Clasificacion,CommonErrorMessage,Documento
} from "./entidad.indice";

import { GestorCacheService } from "../../Compartido/GestorCache/gestor-cache.service";

export class MensajeBarraCia {

    private _ListaCategoria : Array<Categoria>;
    private _ListaUsuarioCompania : Array<UsuarioCompania>;
    private _ListaFlujosCompania : Array<Flujo>;
    private _NoCargarCompanias: boolean;
    private _ListaNominaPeriodos : Array<NominaPeriodo>;
    private _ListaCentroCostosPorDefecto : Array<CentroCosto>;
 
    private _ListaSucursales : Array<Sucursal>;
    private _ListaClasificaciones : Array<Clasificacion>;
    private _ListaClasificacionesTipo : Array<Clasificacion>;
    private _Errores : Array<CommonErrorMessage>;
    private _DocumentoDefaultGrupo : Array<Documento>;
    private _ListaBodegas : Array<Bodega>;
    private _TieneComponentesNomina:boolean;

    //COMPANIAS
    private _ListaCompanias: Array<Compania>;
    get ListaCompanias():Array<Compania> {
        return this._ListaCompanias;
    }
    set ListaCompanias(ListaCompanias:Array<Compania>) {

        this._ListaCompanias = ListaCompanias;
    }
    

    //DEPARTAMENTOS
    private _ListaDepartamentos : Array<Departamento>;
    get ListaDepartamentos():Array<Departamento> {

        return this._ListaDepartamentos;
    }
    set ListaDepartamentos(ListaDepartamentos:Array<Departamento>) {

        this._ListaDepartamentos = ListaDepartamentos;
    }

    //PERIODO
    private _ListaPeriodos : Array<Periodo>;
    get ListaPeriodos():Array<Periodo> {

        //Cache - revisar si necesita ser actualizado
        this.gestorCache.refrescaCache();
        return this._ListaPeriodos;
    }
    set ListaPeriodos(ListaPeriodos:Array<Periodo>) {

        this._ListaPeriodos = ListaPeriodos;
    }
    
    //CONSTRUCTOR
    constructor(public gestorCache: GestorCacheService) {

    }
}
