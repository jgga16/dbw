import { ICatalogo } from '../../Menu/Modelo/ICatalogo';

export class Periodo implements ICatalogo {
    IdPeriodo: string;
    Anio: number;
    NumeroPeriodo: number;
    Descripcion: string;
    FechaInicio: string; 
    FechaFin: string; 
    Estado: boolean;
    Contabilizado:boolean;
    EstadoHistoria:boolean;
    Numeracion:number;
    IsCierreActivosFijos:boolean;
    
    Compania: any;  //Navegacion
    IdCompania: string;
    IdPeriodoHistoria: string;
    IdTransaccionCabecera: string;
    TransaccionCabecera: any;   //Navegacion
    TipoNumeracionPorComponente:number;

    IdDocumentoNumeracion:string;

    // getters and setters to implement interface
    get id(): string {
        return this.IdPeriodo;
    }
    set id(value: string) {
        this.IdPeriodo = value;
    }
    get nombre(): string {
        return this.Descripcion;
    }
    set nombre(value: string) {
        this.Descripcion = value;
    }
    get codigo(): string {
        return this.IdPeriodo;
    }
    set codigo(value: string) {
        this.IdPeriodo = value;
    }


}