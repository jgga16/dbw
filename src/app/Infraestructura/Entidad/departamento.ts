export class Departamento  {
	IdDepartamento: string;
	IdGrupo: string;
	IdDepartamentoPadre: string;
	NombreDepartamento: string;
	Estado: boolean;
	Nivel: number;
	SiglasDepartamento: string;
	IdDepartamentoHistoria: string;
	EstadoHistoria: boolean;
	IdTransaccionCabecera: string;
	IsDeleted: boolean;
    Movimiento: boolean;
    
    /*
	DocumentoSecuencias: Array<DocumentoSecuencia>;
	UsuarioDepartamentos: Array<UsuarioDepartamento>;
	GrupoCompania: GrupoCompania;
	Usuarios: Array<Usuario>;
	TipoNumeracionPorComponente: TipoNumeracionPorComponenteEnum;
	IdDocumentoNumeracion: string;
	NombrePadre: string;
	Default: boolean;
	TieneHijos: boolean;
	TieneMovimientos: boolean;
    BarraEstadoVerde: BarraEstadoVerde;
    */
    
}

