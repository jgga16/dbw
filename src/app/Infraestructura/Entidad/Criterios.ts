

export class Criterios  {
	Campo: string;
	Tipo: TipoDatoCriterio;
	Operador: OperadorBusqueda;
	Valor: string;
	EsDetalle: boolean;
	ListaValor: Array<string>;
	CampoVariable1: string;
	IsSubreporte: number;
}

