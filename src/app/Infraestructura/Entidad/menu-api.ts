// Class used by the main level menu to display each of the menu sections
export class DbWareMenuApi {

    IdMenu: String;
    Nombre: String;
    Icon: String;
    Orden: Number;
    DescripcionProceso:string; //Unused
    Hijos: Array<DbWareMenuApi>
    MenuDB: any; //Unused
    IsShown: Boolean;
    Disable: Boolean;

    constructor(givenId: String, givenOrder:Number, givenIco: String, givenName: String,
        givenChil: Array<DbWareMenuApi>) {
        this.IdMenu = givenId;
        this.Orden = givenOrder;
        this.Icon = givenIco;
        this.Nombre = givenName;
        this.Hijos = givenChil;
        this.IsShown = false;
        this.Disable = false;
    }
}