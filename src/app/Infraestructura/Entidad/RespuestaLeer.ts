import { CommonErrorMessage } from "./common-error-message";


export class RespuestaLeer  {
	Entidades: any[]; //Array<T>;
	TotalRegistros: number;
	Errores: Array<CommonErrorMessage>;
	DatoAdicional: string;
	LlamadoZonaCatalogo: boolean;
	DatoBytesAdicional: any[] //Byte[];
}


