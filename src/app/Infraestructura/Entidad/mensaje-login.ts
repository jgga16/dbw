import { DbWareMenuApi } from "./menu-api";
import { MensajeBarraCia } from "./mensaje-barra-cia";

export class MensajeLogin {
    "FechaProcesos": "";
    "MensajeError": "";
    "MenuApi": Array<DbWareMenuApi>;
    "Usuario": {};
    "Perfil": {};
    "Catalogos": {};
    "SesionHash": "";
    "DatosBarraCia": MensajeBarraCia;
}
