import { Transaccion, CommonErrorMessage } from "./entidad.indice";
import { EstadoComponente } from "./estadocomponente";

//RESPUESTA DEL SERVIDOR DE BDD
export class RespuestaGrabarTransaccion  {
	IdTab: number;
	TransaccionAGrabar: Transaccion
	Errores: CommonErrorMessage[]; //Array<CommonErrorMessage>;
	IdAuxiliar: string;
	ErroresFe: Array<CommonErrorMessage>;
}


//RESPUESTA DEL GESTOR DE ERRORES HACIA LOS COMPONENTE (crado por mi)
export class ComponenteErrorMessage  {
	EstadoComponente: EstadoComponente;
	Errores: CommonErrorMessage[]; //Array<CommonErrorMessage>;
}
