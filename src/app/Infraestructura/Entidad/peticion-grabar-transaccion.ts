import { Transaccion } from "./transaccion";

export class PeticionGrabarTransaccion  {
	IdProceso: string;
	Hash: string;
	Conexion: string;
	TransaccionAGrabar: Transaccion;
	IdAuxiliar: string;
}

