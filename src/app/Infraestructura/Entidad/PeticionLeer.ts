import { Criterios } from "./Criterios";

export class PeticionLeer  {
	SortOrder: SortOrder;
	identificadorAuxiliar: number;
	IsSubreporte: number;
	NombreReporte: string;
	Folder: string;
	EsCliente: boolean;
	ListaGuids: Array<string>;
	Criterios: Array<Criterios>;
	NumeroPagina: number;
	TamanoPagina: number;
	CampoOrdenar: string;
	Hash: string;
	IsRefrescarCombosComponente: boolean;
	EquivalenciasListIdTransaccion: any[]; //Array<KeyValuePair>;
	EquivalenciasIdTransaccionCabecera: string;
	EsCopia: boolean;
	ExtensionArchivo: string;
	Ids: string;
	IdUsuario: string;
	IdAuxiliar: string;
	TipoCatalogo: CatalogosType;
}

