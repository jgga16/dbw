export class TransaccionCabecera  {
	IdTransaccionCabecera: string;
	IdCompania: string;
	IdUsuario: string;

	IsPendienteFe: boolean;
	Siglas: string;
	Numero: number;
	IdProceso: string;

	IdProcesoHistoria: string;

	Fecha: string; //DateTimeOffset;
	FechaValor: string; //dateTime;

	ContadorImpresion: number;
	
	Anio: number;
	Periodo: number;
	Dia: number;

	IdUsuarioHistoria: string;
	IdSriAutorizacionCabecera: string;
	IdSriPuntoEmision: string;
	IdSriPuntoEmisionHistoria: string;
	IsTrsReversa: boolean;
	IsAnulado: boolean;
	IsCopiaAutomatica: boolean;
	IsReversado: boolean;
	AnuladoInterno: boolean;




	/*

	Descripcion: string;
	Campo1: string;
	Campo2: string;
	Campo3: string;
	Campo4: string;
	Campo5: string;
	Campo6: string;
	Campo7: string;
	Campo8: string;
	Campo9: string;
	Campo10: string;
	Opcion1: number;
	Opcion2: number;
	Opcion3: number;
	Numero1: number;
	Numero2: number;
	Numero3: number;
	Fecha1: dateTime;
	Fecha2: dateTime;
	ValorOriginal: number;
	ValorProcesado: number;
	ValorAnulado: number;
	ValorSaldo: number;
	RegistrosOriginal: number;
	RegistrosProcesado: number;
	RegistrosPendiente: number;
	IdCategoriaGrupoCia1: string;
	IdCategoriaGrupoCia2: string;
	IdCategoriaGrupoCia3: string;
	IdCategoriaGrupoCia4: string;
	IdCategoriaGrupoCia5: string;
	IdCategoriaGrupoCia1Historia: string;
	IdCategoriaGrupoCia2Historia: string;
	IdCategoriaGrupoCia3Historia: string;
	IdCategoriaGrupoCia4Historia: string;
	IdCategoriaGrupoCia5Historia: string;

	NumeroDias: number;
	FechaVencimiento: dateTime;
	IdDepartamento: string;
	IdDepartamentoHistoria: string;
	IdDepartamentoUsuario: string;
	IdDepartamentoUsuarioHistoria: string;
	IdSucursalUsuario: string;
	IdSucursalUsuarioHistoria: string;

	PuntoEmision: string;
	NumeroAutorizacion: string;
	CodigoSri: string;

	IdDocumento: string;
	IdDocumentoHistoria: string;
	Opcion4: number;
	Opcion5: number;
	Opcion6: number;
	Opcion7: number;
	Opcion8: number;
	Opcion9: number;
	Opcion10: number;
	Numero4: number;
	Numero5: number;
	Numero6: number;
	Numero7: number;
	Numero8: number;
	Numero9: number;
	Numero10: number;
	Fecha3: dateTime;
	Fecha4: dateTime;
	Fecha5: dateTime;
	Fecha6: dateTime;
	Fecha7: dateTime;
	Fecha8: dateTime;
	Fecha9: dateTime;
	Fecha10: dateTime;

	ValorControlOPSOrigen: number;
	ValorControlOPSProcesado: number;

	IdTraza: string;
	IdTransaccionCabeceraCopiado: string;
	CodigoDepartamento: string;
	CodigoSucursal: string;
	FechaFinal: DateTimeOffset?;
	IdUsuarioPersonalizado: string;
	Texto01: string;
	Texto02: string;
	Texto03: string;
	Id01: string;
	Id02: string;
	Id03: string;
	Texto04: string;
	Texto05: string;
	Texto06: string;


	MotivoReversa: string;
	MotivoReversaFlujo: string;
	IsReprocesado: boolean?;
	IsOrigenReverso: boolean?;
	
	IdTransaccionOrigenReverso: string;

	Periodoes: Array<Periodo>;
	TransaccionDetalles: Array<TransaccionDetalle>;
	DiarioCabeceras: Array<DiarioCabecera>;
	SriAutorizacionRupturas: Array<SriAutorizacionRuptura>;
	Proceso: Proceso;
	Compania: Compania;
	Flujoes: Array<Flujo>;
	TransaccionCabeceraRmpPrc: Array<TransaccionCabeceraRmp>;
	TransaccionCabeceraRmpOri: Array<TransaccionCabeceraRmp>;
	TipoNumeracionPorComponente: TipoNumeracionPorComponenteEnum;
	IdDocumentoNumeracion: string;
	NombreProcesoImpresion: string;
	DocumentoCatalogo: ICatalogo;
	CategoriaGrupoCia1Catalogo: ICatalogo;
	CategoriaGrupoCia2Catalogo: ICatalogo;
	CategoriaGrupoCia3Catalogo: ICatalogo;
	CategoriaGrupoCia4Catalogo: ICatalogo;
	CategoriaGrupoCia5Catalogo: ICatalogo;
	DepartamentoCatalogo: ICatalogo;
	SucursalCatalogo: ICatalogo;
	CompaniaCatalogo: ICatalogo;
	UsuarioCatalogo: ICatalogo;
	CategoriaTitulo1: string;
	CategoriaTitulo2: string;
	CategoriaTitulo3: string;
	CategoriaTitulo4: string;
	CategoriaTitulo5: string;
	TipoFechaValor: number;
	IdTransaccionesRelacionadas: Array<Guid>;
	BarraEstadoVerde: BarraEstadoVerde;
	EsReproceso: boolean;
	TransaccionDetallesImpresion: Array<TransaccionDetalle>;
	*/
}

