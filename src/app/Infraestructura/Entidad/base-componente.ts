import { Input, OnDestroy } from "@angular/core"
//ENTIDADES
// import { EstadoComponente, RespuestaGrabarTransaccion, CommonErrorMessage } from "./entidad.indice";

import { EstadoComponente } from "./estadocomponente";
import { RespuestaGrabarTransaccion } from "./respuesta-grabar-transaccion";
import { CommonErrorMessage } from "./common-error-message";

//SERVICIOS INTERFASE
import { TabService } from "../../Menu/tab/tab.service";

//SERVICIOS SHELL
import { CoordinacionService } from "../Shell/Servicio/servicio.indice";
import { Subscription, Observable } from "rxjs"
import { CoordinacionMensaje } from "../Shell/Modelo/shell-modelo.indice";

export class BaseComponente implements OnDestroy {

    //ENTRADA SALUDA
    @Input() idTab:number;
  
    //ESTADO DEL COMPONENTE
    _estadoComponente: EstadoComponente;
    @Input()
    set estadoComponente(value: EstadoComponente){
      //console.log("Asigno componente ", value);
      this._estadoComponente = value;
    }
    get estadoComponente(): EstadoComponente {
        return this._estadoComponente;
    }
    constructor() {
    }
    
    //ERRORES
    public Errores: CommonErrorMessage[] = [];

    //COMUNICACIONES
    protected coordinacionMensaje: CoordinacionMensaje = new CoordinacionMensaje();


    //SUBCRIPCIONES
    //SUBSCRIPCIN TAB
    public refTab: Subscription;

    //SUBSCRIPCIN COORDINADOR
    public refRespuesta: Subscription;
    public Respuesta$: Observable<RespuestaGrabarTransaccion>;

    ngOnDestroy() {
        //Desubscribe respuestaTransaccion en CoordinacionService
        // if(this.refRespuestaTransaccion) 
          this.refRespuesta.unsubscribe();
          this.refTab.unsubscribe();
      }

  

    cambiaEstado(estado:string):boolean {
      if (estado==EstadoComponenteTipo.editar) {
        this.estadoComponente.estado = "editar";
        return true;
      }

        if (estado==EstadoComponenteTipo.readonly) {
          this.estadoComponente.estado = "readOnly";
          return true;
        }
    
        if (estado==EstadoComponenteTipo.error  ) {
          this.estadoComponente.estado = "Error";
          return true;
        }


        console.log("Estado del COMPONENTE: no definido");
        return false;
    }
     

  
}
