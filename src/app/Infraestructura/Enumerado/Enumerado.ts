


/* ESTADOS BOTONERA PRINCIPAL */
const enum BotoneraEstado
{
    Inicial,
    Grabacion,
    Extendido   //Inicial + Otro Botones
}


/* ESTADOS DEL COMPONENTE */
const enum EstadoComponenteTipo
{
    editar='editar',
    readonly='readonly',
    error='error'
}

//# TIPOS DE ERRORES DEVUELTOS POR EL SERVIDOR
const enum CommonErrorType
{
    Required,
    InvalidFormat,
    NumericExpected,
    TimeOut,
    Concurrency,
    NotFound,
    Warning,
    NotRequired,
    Information,
    NoError,
    NotEqual,
}

const enum TipoMensajeError
{
    ErrorReporte = 1,
    ErrorLecturaDatos = 2,
    ErrorGrabacionDatos = 3,
    ErrorLogin = 4,
    ErrorTimeOut = 5,
    ErrorComunicacion = 6,
    ErrorAdministrador = 7,
    ErrorExpiracion = 8
}
//# TIPOS DE ERRORES DEVUELTOS POR EL SERVIDOR





const enum TipoDatoCriterio
{
    Entero,
    Texto,
    TextoNull,
    Boleano,
    Guid,
    DateTime,
    Decimal
}


const enum OperadorBusqueda
{
    Igual,
    IniciaCon,
    Contiene,
    Is,
    DiferenteDe,
    MayorIgual,
    MenorIgual,
    In
}

const enum SortOrder
{
    Ascending = 0,     /// A -> Z or 0 -> 10
    Descending = 1     /// Z -> A or 10 -> 0

}

const enum CatalogosType
    {
        None,
        Usuario,
        Sucursal,
        GrupoCompania,
        Compania,
        Perfil,
        Departamento,
        Bodega,
        Clasificacion,
        ClasificacionCompania,
        Categoria,
        CategoriaGrupoCia,
        Banco,
        BancoCash,
        CashManagementBeneficiario,
        Cliente,
        Proveedor,
        CuentasContables,
        CentrosCosto,
        Item,
        ContactoTipo,
        Periodo,
        Job,
        SriPuntoEmision,
        SriTipoDocumento,
        SriAutorizacionCabecera,
        Documento,
        ImpuestoLegal,
        CuentaBanco,
        CuentaBancoEmpleado,
        SriRetencionIar,
        UnidadMedida,
        SriTipoComprobante,
        SriSustentoTributario,
        SriTipoIva,
        SriRetencionIva,
        CatalogosCompaniaConfiguracion,
        Proceso,
        Flujos,
        Componente,
        TipoTarjetaCredito,
        TarjetaCredito,
        ComponenteCampos,
        ProcesoPrecedente,
        NominaVariable,
        NominaPeriodo,
        MaestroEmpleado,
        NominaColumna,
        ConfigurarIngresoDatos,
        ConfigurarIngresoDatosColumna,
        ConfiguracionPlantillaExcel,
        ColumnaConfiguracionPlantillaExcel,
        NominaListar,
        NominaListarRol,
        NominaListarDatosColumna,
        NominaListarColumnas,
        UsuarioDepartamento,
        UsuarioCompania,
        UsuarioBodega,
        UnidadProduccion,
        MetodoDepreciacion,
        ActivoFijoMaestro,
        ActivoFijoImagen,
        ProcesoBatch,
        FormatoImpresion,
        FormatoImpresionProceso,
        ProcesoAutomatico,
        NominaRol,
        EncuestaPlantilla,
        ListarProcesos,
        ProcesoBSCConfiguracion,
        DefinicionBSC,
        ReporteBSCConfiguracion,
        PaisSri,
        ProcesarNomina,
        NominaColumnaPeriodo,
        FormasdePago,
        Pais
    }

const enum TipoFiltroCatalogo
    {
        Codigo,
        Nombre,
    }

const enum EnumTipoCatalogoArbol
    {
        Normal,
        Arbol
    }



//# Configuracion Visual
const enum ShowModeType
{
    ReadOnly,
    Editable,
}

const enum TipoVisibleEnum
{
    NoVisible,
    VisibleEnCabecera,
    VisibleEnDetalle
}

const enum TipoVisibleCortoEnum
{
    NoVisible,
    Visible
}

//tipo dato ComponenteCampo
const enum TipoDatoCampoEnum
{
    Objeto,
    String,
    DateTime,
    Decimal,
    Boolean,
    Int,
    Short,
    Guid
}
//# Configuracion Visual

