import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { AutenticacionService } from '../../Servicio/autenticacion.service';
import { Login } from '../../../Entidad/login';




import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
 
})
export class LoginComponent implements OnInit, OnDestroy {
  public Login: Login;
  public progreso:number=0;
  private refestaAutenticado: Subscription = null;

  constructor(private Autentica: AutenticacionService, private Ruteador: Router) { 

    this.Login = new Login();
    this.Login.dominio = 'Takeda';
    this.Login.login = 'Dbweb';
    this.Login.clave = 'Admin7410*';
    this.Login.uIP = this.Autentica.ipLocal; 
  }

  ngOnInit() {
    this.suscribirse();
  }

  suscribirse():void {
      //Suscribire al servicio de authenticacion
      this.refestaAutenticado = this.Autentica.estaAutenticado$.subscribe(

        res => {
          console.log("LoginComponent => estaAutenticado$ => ",res);
          this.progreso = 100;
          this.Ruteador.navigate(['/principal']);
        },
        err => {
          this.progreso = 100;
          console.log("Ocurrio un error.");
        },
        
        () => {
          this.progreso = 100;
           console.log('Se completo la notificacion.')
        }
      )
  }

  ngOnDestroy() {
    //Dessubscribir al observable del servicio de autenticacion
    // if(this.refestaAutenticado) 
      this.refestaAutenticado.unsubscribe();
    
    
  }


  Ingreso():void {
    this.progreso = 20;
    this.Autentica.Autentica(this.Login);
  }

  Salida():void {
    this.progreso = 20;
    //this.Autentica.MensajeLogin=null;
    this.Autentica.mensaje= "Salio del sistema";
    this.progreso = 100;
 
  }


}
