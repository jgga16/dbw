import { ConfiguracionComponente } from "./shell-modelo.indice";
import { AccionMensaje } from "./accion-mensaje";
import { EstadoComponente } from "../../Entidad/estadocomponente";
import { CommonErrorMessage } from "../../Entidad/entidad.indice";


export class CoordinacionMensaje {
    Accion: AccionMensaje;   //idTab,accion, tipoProceso
    EstadoComponente: EstadoComponente;
    Dato: any;
    Errores: CommonErrorMessage[]=[];
}


