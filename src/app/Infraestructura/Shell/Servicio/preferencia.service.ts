import { Injectable } from '@angular/core';
import { GestorCacheService } from '../../../Compartido/GestorCache/gestor-cache.service';
import { CONSTANTE } from '../../Constante/constante';

@Injectable({
  providedIn: 'root'
})
export class PreferenciaService {
 

 IdAmbiente(Id:string,tipo:string):string {
   var IdAmbiente:string;
   switch (tipo) {
     case "Compania": {
      IdAmbiente = Id +  "-IdCompania-";
      IdAmbiente = IdAmbiente + this.gestorCache.DatosBarraCia.ListaCompanias[CONSTANTE.INDICE_PRIMERA_COMPANIA].IdCompania;
      break;
     }
     case "Grupo": {
      IdAmbiente = Id +  "-IdGrupo-";
      IdAmbiente = IdAmbiente + this.gestorCache.DatosBarraCia.ListaCompanias[CONSTANTE.INDICE_PRIMERA_COMPANIA].IdGrupo;
      break;
     }
   } //fin swith
   return IdAmbiente;

 }

 guarda(tipo: string, clave:string, objeto: any):void {
  var IdAmbienteClave = this.IdAmbiente(clave,tipo); 
  localStorage.setItem(IdAmbienteClave, JSON.stringify(objeto));
 }

 obtiene(tipo: string, clave:string): any {
  var IdAmbienteClave = this.IdAmbiente(clave,tipo); 
  //alert('clave-ambiente ' + IdAmbienteClave);
  let objeto = JSON.parse(localStorage.getItem(IdAmbienteClave));
  return objeto;
 }

  constructor( public gestorCache: GestorCacheService) { 
   

  }
}
