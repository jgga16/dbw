import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable,  Subject, observable, BehaviorSubject } from 'rxjs';

import { Login } from '../../Entidad/login';

//Servicio de configuracion
import { ConfiguracionService } from './configuracion.service';
import { PeticionGrabarTransaccion, RespuestaGrabarTransaccion, CommonErrorMessage, PeticionLeer } from '../../Entidad/entidad.indice';


//Pruebas porque no esta la api


@Injectable({
  providedIn: 'root'
})

export class BddService {

   
  constructor(public http: HttpClient) { }

  //DVUELVE UN OBSERVABLE
  getLogin(pLogin: Login):Observable<any> {

    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    let URL:string= ConfiguracionService.URLbase + "/api/login";

    return this.http.post<string>(URL, pLogin, httpOptions).pipe();
     
      //return this.mensajeLogin;
  } //funcion getLogin

 

  //DVUELVE UN OBSERVABLE
  leerCatalogoPaginado(peticionLeer: PeticionLeer):Observable<any> {
    debugger;
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    let URL:string= ConfiguracionService.URLbase + "/api/leerCatalogo";

    return this.http.post<string>(URL, peticionLeer, httpOptions).pipe();

  } //funcion leerCatalogoPaginado





  // grabarTransaccion(mensajaGrabacion: PeticionGrabarTransaccion): Observable<any> {
  //   const httpOptions = {
  //     headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  //   };
  //   let URL:string= ConfiguracionService.URLbase + "/api/GrabarTransaccion";
  //   return this.http.post<string>(URL, mensajaGrabacion, httpOptions).pipe();
  // } //grabarTransaccion




  grabarTransaccion(idTab:number, mensajaGrabacion: PeticionGrabarTransaccion): Observable<RespuestaGrabarTransaccion> {
    
    //Notificaion de la respuesta
    let respuesta: RespuestaGrabarTransaccion = new RespuestaGrabarTransaccion();
    let respuestaSubject = new BehaviorSubject(respuesta);
    let respuestaTransaccion$: Observable<RespuestaGrabarTransaccion> =  respuestaSubject.asObservable();
    

    //Mensaje de error de prueba
    let errorUno: CommonErrorMessage = new CommonErrorMessage();
    errorUno.ErrorType = CommonErrorType.Required;
    errorUno.ErrorText = "El camo es obligatorio en la BDD";
    errorUno.ErrorProperty = "propiedad";
    
    //Respuesta de la WebApi
    
    respuesta.IdTab = idTab;
    respuesta.IdAuxiliar= "123456";
    respuesta.Errores =[];
    respuesta.Errores.push(errorUno);

    // console.log("   **** bdd-srv => enviando Web API")
    
     return respuestaTransaccion$;

  } //grabarTransaccion





}
