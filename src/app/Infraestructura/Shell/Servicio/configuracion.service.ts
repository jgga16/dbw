import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
//CLASE GLOBAL DE CONFIGURACIONES - NO SE INJECTA
export abstract class ConfiguracionService {
  
  private static _URLbase:string = "http://localhost:12912";

  static get URLbase(){
     return this._URLbase;
  }
  static set URLbase(URLbase:string){
    this._URLbase = URLbase;
  }

}



