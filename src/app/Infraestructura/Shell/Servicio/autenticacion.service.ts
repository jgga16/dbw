//Librerias
import { Injectable, OnInit, OnDestroy } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable,  Subject, Subscription, BehaviorSubject } from 'rxjs';

//Modelo
import { Login,MensajeLogin } from '../../Entidad/entidad.indice';


//Servicio de configuracion
import { ConfiguracionService } from './configuracion.service';
import { GestorCacheService } from '../../../Compartido/GestorCache/gestor-cache.service';
import { BddService } from './bdd.service';



@Injectable({
  providedIn: 'root'
})
export class AutenticacionService implements OnInit, OnDestroy {
  
  //Notifica si esta autenticado  

  private _Login: Login;
  private _Mensaje: string;
  
  private _ipLocal: string;

  private _estaAutenticadoSubject = new BehaviorSubject(false);  
  public estaAutenticado$: Observable<boolean> = this._estaAutenticadoSubject.asObservable();


  public refMensajeLogin: Subscription = null;

  constructor( public BddService: BddService, public gestorCache: GestorCacheService) {
  };

  public mensajeLogin$: Observable<any>; // = new Observable<MensajeLogin>();



  set Login(Login:Login) {
    this._Login = Login;
  }
  get Login(): Login {
    return this._Login;
  }


  set ipLocal(ipLocal:string) {
    this._ipLocal = ipLocal;
  }
  get ipLocal(): string {
    this._ipLocal=  window.location.hostname;  //window.location.origin;
    return this._ipLocal;
  }

  set mensaje(Mensaje:string) {
    this._Mensaje = Mensaje;
  }
  get mensaje(): string {
    return this._Mensaje;
  }

  ngOnInit(){
    this.suscribirse();
  };

  ngOnDestroy() {
    //Dessubscribir al observable del servicio de autenticacion
    // if(this.refMensajeLogin) 
      this.refMensajeLogin.unsubscribe();
  }

  
  suscribirse():void {
  
    //Suscribire al servicio de authenticacion
    this.refMensajeLogin = this.mensajeLogin$.subscribe(
      res => {
            alert('AutenticacionService => subscripcion');

            let mensaje: MensajeLogin;
            mensaje= (JSON.parse(res) as MensajeLogin); 

            console.log("Mensaje Original 1==>", mensaje );

            //Guardo en cache local
            this.gestorCache.SesionHash = mensaje.SesionHash;
            this.gestorCache.FechaProcesos = mensaje.FechaProcesos;
            this.gestorCache.MensajeError =mensaje.MensajeError;
            this.gestorCache.Usuario =mensaje.Usuario;
            this.gestorCache.Perfil =mensaje.Perfil;
            this.gestorCache.Catalogos =mensaje.Catalogos;
            this.gestorCache.DatosBarraCia =mensaje.DatosBarraCia;
            this.gestorCache.MenuApi = mensaje.MenuApi;

            //Notifica que esta autenticado: login
            console.log("AutenticacionService => SesionHash => ", mensaje.SesionHash);
            if (mensaje.SesionHash)
               this._estaAutenticadoSubject.next(true);
            else  {}
               this._estaAutenticadoSubject.next(false);
           
      },
      err => {

             this._estaAutenticadoSubject.complete();

        this.mensaje = "Autentica Error: no se pudo conectar al servicio de datos";
        console.log(this.mensaje);
        //this.mensajeLoginSuject.complete();

      }
    );



}


  Autentica (pLogin: Login): void {
    
    this.Login = pLogin;
    this.mensaje ="";

    //OPbservable al cual se encuentra subscrito login
    this.mensajeLogin$ = this.BddService.getLogin(pLogin);

    //Suscribire mensajeLogin
    this.suscribirse();


  }    



  




}
