export { CatalogoService } from "./catalogo.service";

export { GestorErrorService } from "./gestor-error.service";

export { PreferenciaService } from "./preferencia.service";
export { ConfiguracionService } from "./configuracion.service";
export { AutenticacionService } from "./autenticacion.service";
export { BddService } from "./bdd.service";
export { CoordinacionService } from "./coordinacion.service";




