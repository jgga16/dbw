import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { CoordinacionMensaje } from '../Modelo/shell-modelo.indice';
import { BddService } from './servicio.indice';
import { PeticionGrabarTransaccion, RespuestaGrabarTransaccion, CommonErrorMessage, Transaccion, EstadoComponente } from '../../Entidad/entidad.indice';
import { Observable, Subscription, BehaviorSubject, Subject } from 'rxjs';
import { CONSTANTE } from '../../Constante/constante';
import { GestorErrorService } from './gestor-error.service';
import { GestorCacheService } from '../../../Compartido/GestorCache/gestor-cache.service';

@Injectable({
  providedIn: 'root'
})
export class CoordinacionService implements OnInit, OnDestroy {

  //Mensaje de RECIBIDO (RespuestaGrabarTransaccion) => del servicio de bdd
  public recibe: RespuestaGrabarTransaccion = new RespuestaGrabarTransaccion();
  public recibe$: Observable<any>; // = new Observable<any>();
  public refrecibe: Subscription = null;

  //Para RESPONDER => A los componentes
  public _respuestaSubject = new Subject<RespuestaGrabarTransaccion>();
  public Respuesta$ = this._respuestaSubject.asObservable();

  //Capta los mensajes de coordinacion  => CADA COMPONENTE
  private colaCoordinacion: CoordinacionMensaje[]=[];
  private procesoEstado: boolean[]=[];
  
  constructor(public bddService: BddService,public gestorErrorSrv: GestorErrorService, public gestorCacheSrv:GestorCacheService) { }


  ngOnInit(){
    
  };

  ngOnDestroy() {
    //Dessubscribir al observable del servicio
    // if(this.refrecibe) 
      this.refrecibe.unsubscribe();
  }
//Gestion de Errores

//Crear proceso a coordinar
  creaProcesoEstado(idProceso: string,proceso: EstadoComponente[]):boolean {
    //IMPLEMENTAR UN DICCIONARIO DE PROCESOS <idproceso, EstadoComponente[]>

    //COMONENTES RENTREGADOS AL COORDINADOR DEBE CREARSE EL MOMENTO DE CREAR LOS TAB
    
    this.procesoEstado[CONSTANTE.ID_CMP_FACTURA]= false;
    this.procesoEstado[CONSTANTE.ID_CMP_SRI]= false;
    this.procesoEstado[CONSTANTE.ID_CMP_TRSC]= false;

    proceso.forEach((componente,indice) => {
      this.procesoEstado[indice]=false;
    });

    return true;
  }

  obtenerComponentes(idTab:number): CoordinacionMensaje[] {
    let componentes : CoordinacionMensaje[]=[];
    componentes=this.colaCoordinacion.filter( coordinacionMensaje => coordinacionMensaje.EstadoComponente.idTab == idTab);
    return componentes;

  }
  estaProcesoCompleto() {
    if ( this.procesoEstado.every(this.estaPresente))
        return true;
    else 
        
        return false; 
    
  }
  
  estaPresente(presente:boolean) {
    return presente==true;
  }
  

  esProcesoValido(idTab: number):boolean {
    let componentes:  CoordinacionMensaje[]=[];

    // componentes=this.colaCoordinacion.filter( coordinacionMensaje => coordinacionMensaje.EstadoComponente.idTab == idTab);
    this.colaCoordinacion.forEach(coordinacionMensaje => {
      if (coordinacionMensaje.EstadoComponente.idTab==idTab) {
        componentes.push(coordinacionMensaje);
      }
    });
    

    if (componentes.length<=0) return;

    if ( componentes.every(this.sinErrores))
       if (this.estaProcesoCompleto())
          return true
       else    
          return false;
    else 
       
       return false; 
  }

  sinErrores(coordinacionMensaje:CoordinacionMensaje) {
    return coordinacionMensaje.Errores.length<=0;
  }
  
  


//RECEPCION DEL SERVICIO DE BDD 

suscribirseRecibe():void {
  this.refrecibe = this.recibe$.subscribe(
    res => this.resRecibe(res),
    err => this.errRecibe(err),
    () => this.comRecibe()
  );
}

  resRecibe(res) {

      //this.recibe = (JSON.parse(res) as RespuestaGrabarTransaccion); 
      this.recibe = res;

      //RESPUESTA
      console.log('        *** Coordinacion-Srv => Respuesta ' , this.recibe);
      this._respuestaSubject.next(this.recibe);

  }

  errRecibe(err) {
      this._respuestaSubject.error('Este es un error de la WebApi en servidor');
      console.log("Coordinacion-Service: no se obtuvo respuesta del servicio de datos");
  }

  comRecibe() {
      console.log("Coordinacion-Service: el flujo se completo");
      this._respuestaSubject.complete();

  }


  /****************************************
      PETICION AL SERVICION DE BDD
  ************************************* */
  creaPeticion(idTab:number): PeticionGrabarTransaccion {

    let componentesProceso : CoordinacionMensaje[]=[];
    componentesProceso = this.obtenerComponentes(idTab);


    let peticionGrabar: PeticionGrabarTransaccion;
    peticionGrabar = new PeticionGrabarTransaccion();

    peticionGrabar.Hash = this.gestorCacheSrv.SesionHash;
    peticionGrabar.IdProceso = '';
    peticionGrabar.Conexion='';
    peticionGrabar.IdAuxiliar='';

    //Armar transaccion
    peticionGrabar.TransaccionAGrabar=this.creaTransaccion(componentesProceso);
    return peticionGrabar;
  }


  creaTransaccion(componentesProceso:CoordinacionMensaje[]): Transaccion {
    
    let transaccion: Transaccion;
    transaccion = new Transaccion();
    transaccion.BSCConfiguracion=null;
    transaccion.NombrePropiedadesDetalle=null;
    transaccion.TransaccionCabecera=null;
    
    componentesProceso.forEach(p => {
      // p.Dato
    });


    return transaccion;
  }  

  //Envia mensajeGrabacion => servicio BDD 
  enviaPeticion(mensajeGrabacion: PeticionGrabarTransaccion,idTab:number):boolean {
   
    this.recibe$ = this.bddService.grabarTransaccion(idTab,mensajeGrabacion);
    //console.log("          **** bdd-srv => repuesta WEB-API", this.recibe$);
    this.suscribirseRecibe();
 
    return true;
  }

  tieneErrorComponente(coordinacionMensaje: CoordinacionMensaje){
    if (coordinacionMensaje.Errores.length>0) {
      //entregar el error al gestor de errores
        return true;
    } else {

      return false;
    }

   }

 
  //Capta el Mensaje Coordina => de CADA componente
  captarComponente(coordinacionMensaje: CoordinacionMensaje) {
    
    /***********************************************************************
      1.- VALIDACION DE MENSAJE DE LOS COMPONENTES EN CONJUNTO
    ***********************************************************************/
    
    let idComponente = coordinacionMensaje.EstadoComponente.idComponente;
    let accion = coordinacionMensaje.Accion.accion;
    let idTab =   coordinacionMensaje.Accion.idTab;
    console.log('          *** Coordinacion-Srv => IdTab-IdComponente-Accion ' + idTab + '-' + idComponente +  '-' + accion, coordinacionMensaje);
    
    //PROCESA MENSAJE DEL COMPONNETE
    //debugger;

    this.colaCoordinacion[idComponente]=coordinacionMensaje;
    if(this.tieneErrorComponente(coordinacionMensaje))
       this.gestorErrorSrv.agregaError(coordinacionMensaje.EstadoComponente, coordinacionMensaje.Errores);

    /**********************************************************************
      2.- COORDINAR PROCESO DE GRABACION (Armar PeticionGrabarTransaccion)
    ************************************************************************/

    if (this.esProcesoValido(idTab)) {
        /**********************************************************************
         3.- Crear peticion PeticionGrabarTransaccion)
        ************************************************************************/
       let mensajeGrabacion: PeticionGrabarTransaccion;
       mensajeGrabacion = this.creaPeticion(idTab);

        console.log('          *** Coordinacion-Srv => PROCESO VALIDO');
        /**********************************************************************
         3.- ENVIAR AL SERVICIO DE REPOSITORIO (Enviar PeticionGrabarTransaccion)
        ************************************************************************/
        if (this.enviaPeticion(mensajeGrabacion,idTab)) {

        }
      
    } else {
         console.log('          *** Coordinacion-Srv => PROCESO NO VALIDO');

    }





  }


 
 


}
