import { Injectable, OnInit } from '@angular/core';
import { RespuestaLeer } from '../../Entidad/RespuestaLeer';
import { Observable, Subscription, Subject } from '../../../../../node_modules/rxjs';
import { BddService } from './bdd.service';
import { PeticionLeer } from '../../Entidad/entidad.indice';

@Injectable({
  providedIn: 'root'
})
export class CatalogoService implements OnInit {

  //Mensaje de RECIBIDO (RespuestaLeer) => del servicio de bdd
  public recibe: RespuestaLeer;
  public recibe$: Observable<any>;
  public refrecibe: Subscription = null;

  //Para RESPONDER => A los modales
  public _respuestaSubject = new Subject<RespuestaLeer>();
  public Respuesta$ = this._respuestaSubject.asObservable();

  constructor(public bddSrv: BddService) {

  }

  ngOnInit(){
    
  }
  //Metodfos para obtener la informacion

  leerCatalogoPaginado(peticionLeer: PeticionLeer){
    this.recibe$ = this.bddSrv.leerCatalogoPaginado(peticionLeer);
    this.suscribirseRecibe();
  }

  //Recibe respuesta del BDD-Service y Comunica al modal
   suscribirseRecibe():void {
  
    this.refrecibe = this.recibe$.subscribe(
      res => this.resRecibe(res),
      err => this.errRecibe(err),
      () => this.comRecibe()
    );
  }
  
    resRecibe(res) {
        debugger;
        // this.recibe = (JSON.parse(res) as RespuestaLeer); 
        this.recibe = res;
  
        //RESPUESTA
        console.log('        *** CatalogoService => RespuestaLeer ' , this.recibe);
        this._respuestaSubject.next(this.recibe);
  
    }
  
    errRecibe(err) {
      debugger;
        console.log("Catalogo-Service: no se obtuvo respuesta del servicio de datos",err);
        this._respuestaSubject.error(null);
        
    }
  
    comRecibe() {
        console.log("Catalogo-Service: el flujo se completo");
        this._respuestaSubject.complete();
  
    }



}
