import { Injectable } from '@angular/core';
import { CommonErrorMessage, EstadoComponente} from '../../Entidad/entidad.indice';
import { Subject } from 'rxjs';
import { ComponenteErrorMessage } from '../../Entidad/respuesta-grabar-transaccion';



@Injectable({
  providedIn: 'root'
})
export class GestorErrorService {
  
  //Para RESPONDER => A los componentes
  private _respuestaSubject = new Subject<ComponenteErrorMessage[]>();
  public Errores$ = this._respuestaSubject.asObservable();

  //Errores
  public errorProceso: ComponenteErrorMessage[]=[];

  constructor() { }

  agregaError(estadoComponente: EstadoComponente, errorComponente: CommonErrorMessage[]) {

    let componenteErrorMessage:ComponenteErrorMessage;
    componenteErrorMessage = new ComponenteErrorMessage();
    componenteErrorMessage.EstadoComponente = estadoComponente;
    componenteErrorMessage.Errores = errorComponente;

    this.errorProceso.push(componenteErrorMessage);

   }

  eliminaError():boolean {
    this.errorProceso = [];
    return true;
  }

  notificarErrores(IdTab:number) {
    let errorProceso:ComponenteErrorMessage[];
    errorProceso = this.errorProceso.filter(err => err.EstadoComponente.idTab ==IdTab);
    this._respuestaSubject.next(errorProceso);
  }

}
