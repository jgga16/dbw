import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AutenticacionService } from '../Servicio/autenticacion.service';

// @Injectable({
//   providedIn: 'root'
// })
@Injectable()
export class InterceptorService implements HttpInterceptor {

  constructor(public Autenticacion: AutenticacionService) { }
  intercept(request: HttpRequest<any>, siguiente: HttpHandler): Observable<HttpEvent<any>>{
    
    //Si esta autenticado coloca la cabecera con el hash
    // if (this.Autenticacion.MensajeLogin.SesionHash) {
    //   const autenticacionHeader = this.Autenticacion.MensajeLogin.SesionHash;
    //   const headers = request.headers.set('Authorization',autenticacionHeader);
    //   const autenticacionRequest = request.clone({headers: headers})
    //   console.log("Interceptor => SessionHash ",this.Autenticacion.MensajeLogin.SesionHash);
    //   return siguiente.handle(autenticacionRequest);
    // } else { //Si no esta autenticado devuelve el mismo request.
    //   const originalRequest = request.clone();
    //   console.log("Normal SIN Interceptor => SessionHash ",this.Autenticacion.MensajeLogin.SesionHash);
    //   return siguiente.handle(originalRequest);
    // }

    const originalRequest = request.clone();
    return siguiente.handle(originalRequest);


  }
}


