import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AutenticacionService } from '../Servicio/autenticacion.service';
import { GestorCacheService } from '../../../Compartido/GestorCache/gestor-cache.service';



@Injectable({
  providedIn: 'root'
})
export class AutenticaGuard implements CanActivate {

  constructor(public gestorCache: GestorCacheService, private Ruteador: Router) {}


  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

   console.log("Guardian ", this.gestorCache.SesionHash );
    if (this.gestorCache.SesionHash) {
        return true;
      } else {
        console.log("SesionHash Nulo");
        this.Ruteador.navigate(['/login']);
      return false;
      }


  }


}
