import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

//CLASE GLOBAL
import { ConfiguracionService } from './app/Infraestructura/Shell/Servicio/configuracion.service';
import { CONSTANTE } from './app/Infraestructura/Constante/constante';



if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));
